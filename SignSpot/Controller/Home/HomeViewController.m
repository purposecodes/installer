
//
//  HomeViewController.m
//  SignSpot
//
//  Created by Purpose Code on 12/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

typedef enum{
    
    eOrder = 0,
    eRemoveBoard = 2,
    eQuickLook = 1,
    eLogOut = 3
    
}eMenus;



#define kNumberOfCells        4
#define kHeightForHeader      150
#define kHeightForFooter      0.01

#import "HomeViewController.h"
#import "Constants.h"
#import "DashBoardCustomCell.h"
#import "DashBoardCollectionReusableView.h"
#import "CategoriesLayout.h"
#import "OrderListViewController.h"
#import "QuickViewController.h"
#import "AppDelegate.h"

@interface HomeViewController (){
    
    IBOutlet UICollectionView *collectionView;
    IBOutlet UIView *vwNavigation;
    IBOutlet UIButton *btnSlideMenu;
    
  
}

@property (nonatomic) IBOutlet UIBarButtonItem* revealButtonItem;

@end

@implementation HomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUp];
    AppDelegate *delegate = (AppDelegate*) [UIApplication sharedApplication].delegate;
    [delegate enablePushNotification];
   
     
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
}



- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [btnSlideMenu addTarget:self.revealViewController action:@selector( revealToggle:)forControlEvents:UIControlEventTouchUpInside];
        [vwNavigation addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
}

-(void)setUp{
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationController.navigationBarHidden = true;
    
    UINib *nib = [UINib nibWithNibName:@"DashBoardCollectionReusableView" bundle:nil];
    [collectionView registerNib:nib forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"DashBoardCollectionReusableView"];
    
    [collectionView reloadData];
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return kHeightForHeader;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    
   return  kHeightForFooter;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [self showSelectedMenuDetailsWith:indexPath.row];
    
}


#pragma mark - UICollectionViewDataSource Methods

-(NSInteger)collectionView:(UICollectionView *)_collectionView numberOfItemsInSection:(NSInteger)section
{
    return kNumberOfCells;
}

-(DashBoardCustomCell *)collectionView:(UICollectionView *)_collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
 
    DashBoardCustomCell *cell = [_collectionView dequeueReusableCellWithReuseIdentifier:@"DashBoardCustomCell" forIndexPath:indexPath];
    
    if (indexPath.row == eOrder)   {
        cell.lblProductName.text = @"ORDERS";
        cell.templateImage.image = [UIImage imageNamed:@"Order.png"];
    }
    if (indexPath.row == eRemoveBoard){
        cell.lblProductName.text = @"REMOVAL ORDERS";
        cell.templateImage.image = [UIImage imageNamed:@"RemoveOrder.png"];
    }
    if (indexPath.row == eQuickLook) {
        cell.lblProductName.text = @"QUICK VIEW";
        cell.templateImage.image = [UIImage imageNamed:@"QuickView.png"];
    }
    if (indexPath.row == eLogOut) {
        cell.lblProductName.text = @"LOGOUT";
        cell.templateImage.image = [UIImage imageNamed:@"LogoutIcon.png"];
    }

    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)_collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        DashBoardCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"DashBoardCollectionReusableView" forIndexPath:indexPath];
        headerView.lblName.text = [User sharedManager].name;
        headerView.lblLastLoginedTime.text = [User sharedManager].email;
        headerView.imgProfile.layer.cornerRadius = 25;
        headerView.imgProfile.clipsToBounds = YES;
        headerView.imgProfile.layer.borderWidth = 2.f;
        headerView.imgProfile.layer.borderColor = [UIColor colorWithRed:0.97 green:0.67 blue:0.67 alpha:1.0].CGColor;
        headerView.imgProfile.backgroundColor = [UIColor whiteColor];
        if ([User sharedManager].profileurl) {
            [headerView.imgProfile sd_setImageWithURL:[NSURL URLWithString:[User sharedManager].profileurl]
                                                   placeholderImage:[UIImage imageNamed:@"ProfilePlaceHolder.png"]
                                                          completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                          }];

        }
                reusableview = headerView;
    }
    

    return reusableview;
}

- (CGSize)collectionView:(UICollectionView *)_collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    float delta = 0;
    float width = _collectionView.frame.size.width / 2 - delta;
    float height = 200;
    return CGSizeMake(width, height = width);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0; // This is the minimum inter item spacing, can be more
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

#pragma mark - Common Methods

-(void)showSelectedMenuDetailsWith:(NSInteger)menu{
    
    switch (menu) {
        case eOrder:
            [self showOrderListings];
            break;
        case eQuickLook:
            [self mapPageForQuickLook];
            break;
        case eRemoveBoard:
            [self showOrdersWhichAreToBeRemoved];
            break;
            
        case eLogOut:
            [self logOutUser];
            break;
            
        default:
            break;
    }
}

-(void)showOrderListings{
    
    OrderListViewController *orderListPage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:MenuDetailsStoryBoard Identifier:StoryBoardIdentifierForOrderListingPage];
    [self.navigationController pushViewController:orderListPage animated:YES];
}

-(void)mapPageForQuickLook{
    
    QuickViewController *quickView =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:MenuDetailsStoryBoard Identifier:StoryBoardIdentifierForQuickView];
    [self.navigationController pushViewController:quickView animated:YES];
  
}

-(void)showOrdersWhichAreToBeRemoved{
    
    OrderListViewController *orderListPage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:MenuDetailsStoryBoard Identifier:StoryBoardIdentifierForOrderListingPage];
    orderListPage.isRemovalOrder = YES;
    [self.navigationController pushViewController:orderListPage animated:YES];
   
    
}

-(void)logOutUser{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Confirm Logout"
                                  message:@"Logout from the app ?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             [self clearUserSessions];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)clearUserSessions{
    
    [self.navigationController popToRootViewControllerAnimated:NO];
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (defaults && [defaults objectForKey:@"USER"])
        [defaults removeObjectForKey:@"USER"];
    [delegate checkUserStatus];
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];

}


-(void)showLoadingScreenWithTitle:(NSString*)title{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = title;
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}





#pragma mark state preservation / restoration

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Save what you need here
    
    [super encodeRestorableStateWithCoder:coder];
}


- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Restore what you need here
    
    [super decodeRestorableStateWithCoder:coder];
}


- (void)applicationFinishedRestoringState
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Call whatever function you need to visually restore
    [self customSetup];
}


@end
