//
//  CategoriesLayout.m
//  SignInstaller
//
//  Created by Purpose Code on 01/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "CategoriesLayout.h"
#import "OrangeView.h"
#import "UICollectionViewFlowLayout+Helper.h"

@interface CategoriesLayout()
{
    NSMutableDictionary *_layoutAttributes;
    CGSize _contentSize;
}

@end

@implementation CategoriesLayout

- (void)prepareLayout {
    // Registers my decoration views.
    _layoutAttributes = [NSMutableDictionary dictionary]; // 1
    
       NSIndexPath *path = [NSIndexPath indexPathForItem:0 inSection:0];
    
   // [self registerClass:[OrangeView class] forDecorationViewOfKind:@"Vertical"];
    //[self registerClass:[OrangeView class] forDecorationViewOfKind:@"Horizontal"];
    //  UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForSupplementaryViewOfKind:UICollectionElementKindSectionHeader withIndexPath:path];
    
    
}



- (UICollectionViewLayoutAttributes *)layoutAttributesForDecorationViewOfKind:(NSString *)decorationViewKind atIndexPath:(NSIndexPath *)indexPath {
    // Prepare some variables.
    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:indexPath.row+1 inSection:indexPath.section];
    
    UICollectionViewLayoutAttributes *cellAttributes = [self layoutAttributesForItemAtIndexPath:indexPath];
    UICollectionViewLayoutAttributes *nextCellAttributes = [self layoutAttributesForItemAtIndexPath:nextIndexPath];
    
    UICollectionViewLayoutAttributes *layoutAttributes = [UICollectionViewLayoutAttributes layoutAttributesForDecorationViewOfKind:decorationViewKind withIndexPath:indexPath];
    
    CGRect baseFrame = cellAttributes.frame;
    CGRect nextFrame = nextCellAttributes.frame;
    
    CGFloat strokeWidth = 2;
    CGFloat spaceToNextItem = 5;
    if (nextFrame.origin.y == baseFrame.origin.y)
        spaceToNextItem = (nextFrame.origin.x - baseFrame.origin.x - baseFrame.size.width);
    
    if ([decorationViewKind isEqualToString:@"Vertical"]) {
        CGFloat padding = 5;
    
        // Positions the vertical line for this item.
        CGFloat x = baseFrame.origin.x + baseFrame.size.width + (spaceToNextItem - strokeWidth)/2;
        layoutAttributes.frame = CGRectMake(x,
                                            baseFrame.origin.y + padding,
                                            strokeWidth,
                                            baseFrame.size.height - padding*2);
    } else {
        // Positions the horizontal line for this item.
        layoutAttributes.frame = CGRectMake(baseFrame.origin.x,
                                            baseFrame.origin.y + baseFrame.size.height,
                                            baseFrame.size.width + spaceToNextItem,
                                            strokeWidth);
    }
    
    layoutAttributes.zIndex = -1;
    return layoutAttributes;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSArray *baseLayoutAttributes = [super layoutAttributesForElementsInRect:rect];
    NSMutableArray * layoutAttributes = [baseLayoutAttributes mutableCopy];
    
  for (UICollectionViewLayoutAttributes *thisLayoutItem in baseLayoutAttributes) {
            if (thisLayoutItem.representedElementCategory == UICollectionElementCategoryCell) {
                // Adds vertical lines when the item isn't the last in a section or in line.
                if (!([self indexPathLastInSection:thisLayoutItem.indexPath] ||
                      [self indexPathLastInLine:thisLayoutItem.indexPath])) {
                    UICollectionViewLayoutAttributes *newLayoutItem = [self layoutAttributesForDecorationViewOfKind:@"Vertical" atIndexPath:thisLayoutItem.indexPath];
                    [layoutAttributes addObject:newLayoutItem];
                }
                
                // Adds horizontal lines when the item isn't in the last line.
                if (![self indexPathInLastLine:thisLayoutItem.indexPath]) {
                    UICollectionViewLayoutAttributes *newHorizontalLayoutItem = [self layoutAttributesForDecorationViewOfKind:@"Horizontal" atIndexPath:thisLayoutItem.indexPath];
                    [layoutAttributes addObject:newHorizontalLayoutItem];
                }
            } if (thisLayoutItem.representedElementCategory == UICollectionElementCategorySupplementaryView) {
                
               
                
                // Header view
                

            }
        }
    
    for (UICollectionViewLayoutAttributes *layoutAttributes in baseLayoutAttributes) {
        if ([layoutAttributes.representedElementKind isEqualToString:UICollectionElementKindSectionHeader]) {
             NSLog(@"HADER");
        }
    }
    
        
        return layoutAttributes;
    
  
    
   
}



@end
