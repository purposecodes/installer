//
//  OrangeView.m
//  SignInstaller
//
//  Created by Purpose Code on 01/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "OrangeView.h"

@implementation OrangeView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor lightGrayColor];
    }
    
    return self;
}
@end
