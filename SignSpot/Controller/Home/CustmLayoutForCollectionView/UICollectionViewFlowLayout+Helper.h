//
//  UICollectionViewFlowLayout+Helper.h
//  SignInstaller
//
//  Created by Purpose Code on 01/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICollectionViewFlowLayout (Helper)

- (BOOL)indexPathInLastLine:(NSIndexPath *)indexPath ;
- (BOOL)indexPathLastInLine:(NSIndexPath *)indexPath;
- (BOOL)indexPathLastInSection:(NSIndexPath *)indexPath;

@end
