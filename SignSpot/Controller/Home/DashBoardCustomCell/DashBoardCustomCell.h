//
//  ProductCollectionViewCell.h
//  SignSpot
//
//  Created by Purpose Code on 12/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProductCellDelegate <NSObject>

/*!
 *This method is invoked when user taps the 'Close' Button.
 */

/*!
 *This method is invoked when user selects a country.The selected Country Details sends back to Registration page
 */

@end



@interface DashBoardCustomCell : UICollectionViewCell{
    
}

@property (nonatomic, assign) NSInteger row;
@property (nonatomic, weak) IBOutlet UILabel *lblProductName;
@property (nonatomic,weak)  id<ProductCellDelegate>delegate;
@property (nonatomic,weak) IBOutlet UIImageView *templateImage;

-(void)setUp;



@end
