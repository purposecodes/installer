//
//  DashBoardCollectionReusableView.h
//  SignInstaller
//
//  Created by Purpose Code on 01/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashBoardCollectionReusableView : UICollectionReusableView

@property (nonatomic,strong) IBOutlet UILabel *lblName;
@property (nonatomic,strong) IBOutlet UILabel *lblLastLoginedTime;
@property (nonatomic,strong) IBOutlet UIImageView *imgProfile;

@end
