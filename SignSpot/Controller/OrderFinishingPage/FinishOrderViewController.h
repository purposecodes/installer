//
//  FinishOrderViewController.h
//  SignInstaller
//
//  Created by Purpose Code on 02/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FinishOrderViewController : UIViewController

@property (nonatomic,strong) NSDictionary *templateInfo;
@property (nonatomic,assign)BOOL isRemovalOrder;


@end
