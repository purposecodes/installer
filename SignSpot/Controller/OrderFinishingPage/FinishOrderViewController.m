//
//  FinishOrderViewController.m
//  SignInstaller
//
//  Created by Purpose Code on 02/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#define kHeightForHeader        50
#define kHeightForFooter        55
#define kCellHeightForImage     200
#define kCellHeightFordetails   130
#define kNumberOfSections       3

#define kSuccess                200

#import "FinishOrderViewController.h"
#import "PreviewTableViewCell.h"
#import "OrderDetailsTableViewCell.h"
#import "Constants.h"
#import "CustomeImagePicker.h"
#import "PhotoBrowser.h"
#import "CollectionViewCell.h"
#import <Photos/Photos.h>

@interface FinishOrderViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,CustomeImagePickerDelegate,PhotoBrowserDelegate>{
    
    UIImagePickerController *imagePicker;
    IBOutlet UITableView *tableView;
    NSMutableArray *arrImages;
    PhotoBrowser *photoBrowser;
    NSInteger numberOfGrids;
    
    NSMutableArray *arrUploadedNames;
    BOOL isSignBoardRemoved;
    
    
}

@end

@implementation FinishOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    // Do any additional setup after loading the view.
}

-(void)setUp{
    
    numberOfGrids = 0;
    arrImages = [NSMutableArray new];
    arrUploadedNames = [NSMutableArray new];
    self.automaticallyAdjustsScrollViewInsets = NO;
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = NO;
    imagePicker.videoQuality = UIImagePickerControllerQualityTypeLow;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    tableView.estimatedRowHeight = 150;
    tableView.rowHeight = UITableViewAutomaticDimension;
    
    isSignBoardRemoved = true;
    if ([[_templateInfo objectForKey:@"is_removal"] boolValue]) {
        isSignBoardRemoved = false;
    }
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
   
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)showCamera:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                   message:@"Photo"
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"CAMERA"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              [self openCamera];
                                                              [alert dismissViewControllerAnimated:YES completion:^{
                                                                  
                                                              }];
                                                              
                                                              
                                                          }];
    UIAlertAction *second = [UIAlertAction actionWithTitle:@"GALLERY"
                                                     style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                         [self openGallery];
                                                         [alert dismissViewControllerAnimated:YES completion:^{
                                                         }];
                                                     }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"CANCEL"
                                                     style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
                                                         
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    
    [alert addAction:firstAction];
    [alert addAction:second];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];

   
    
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    if (image == nil) {
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    [arrImages addObject:image];
    [imagePicker dismissViewControllerAnimated:YES completion:nil];
    [self updateGalleryCell];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [imagePicker dismissViewControllerAnimated:YES completion:nil];
   
}

-(void)openCamera{
    
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:imagePicker animated:YES completion:nil];
}

-(void)openGallery{
    
    [self showPhotoGallery];
}

-(IBAction)showPhotoGallery
{
    CustomeImagePicker *cip =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:StoryboardForLogin Identifier:StoryBoardIdentifierForImagePicker];
    cip.delegate = self;
    [cip setHideSkipButton:NO];
    [cip setHideNextButton:NO];
    [cip setMaxPhotos:MAX_ALLOWED_PICK];
    
    [self presentViewController:cip animated:YES completion:^{
    }];
}

-(void)imageSelected:(NSArray*)arrayOfGallery isPhoto:(BOOL)isPhoto;
{
   
    float __block width = 300;
    width = self.view.frame.size.width;
    PHImageRequestOptions *requestOptions  = [[PHImageRequestOptions alloc] init];
    requestOptions.synchronous = YES;
    requestOptions.resizeMode = PHImageRequestOptionsResizeModeExact;
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeFastFormat;
    requestOptions.networkAccessAllowed = YES;
    
    __block NSInteger imageCount = 0;
    
    for (PHAsset *asset in arrayOfGallery) {
        
        if (isPhoto) {
            
            PHImageManager *manager = [PHImageManager defaultManager];
            [manager requestImageForAsset:asset
                               targetSize:CGSizeMake(600, 600)
                              contentMode:PHImageContentModeDefault
                                  options:requestOptions
                            resultHandler:^void(UIImage *image, NSDictionary *info) {
                                 [arrImages addObject:image];
                                imageCount ++;
                                if (imageCount == arrayOfGallery.count) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                       [self updateGalleryCell];
                                    });
                                }
                                
                            }];
            
        }
    }
    
}

#pragma mark - UICollectionViewDataSource Methods

-(NSInteger)collectionView:(UICollectionView *)_collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrImages.count;
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)_collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell *cell=[_collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionViewCell" forIndexPath:indexPath];
    
    if (indexPath.row < arrImages.count) {
        cell.imgPreview.image = arrImages[indexPath.row];
        cell.btnCancel.tag = indexPath.row;
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)_collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    float width = (tableView.frame.size.width / 2)  ;
    return CGSizeMake(width,width);
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    
    return 0;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [self presentGalleryWithImages:arrImages andIndex:indexPath.row];
}
#pragma mark - TableView Delegates


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger sections = 2;
    if ([[_templateInfo objectForKey:@"is_removal"] boolValue]) {
        sections += 1;
    }
    return sections;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}



- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PreviewTableViewCell *cell;
    
    if (indexPath.section == 0) {
        static NSString *CellIdentifier = @"Preview";
        if (arrImages.count) {
            PreviewTableViewCell *cell = (PreviewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            [cell.collectionView reloadData];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }else{
            UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"NoImageCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return  cell;
            
        }
        return cell;

    }else if (indexPath.section == 1){
        
        static NSString *CellIdentifier = @"OrderInfo";
        OrderDetailsTableViewCell *cell = (OrderDetailsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
   
        if (NULL_TO_NIL([_templateInfo objectForKey:@"order_duedate"]))
           cell.lblOrderDueDate.text = [NSString stringWithFormat:@"Order Due Date : %@",[_templateInfo objectForKey:@"order_duedate"]];
        
        if (NULL_TO_NIL([_templateInfo objectForKey:@"removal_duedate"]))
            cell.lblOrderDueDate.text = [NSString stringWithFormat:@"Removal Due Date : %@",[_templateInfo objectForKey:@"removal_duedate"]];
        
        if (NULL_TO_NIL([_templateInfo objectForKey:@"order_date"]))
            cell.lblOrderDate.text = [NSString stringWithFormat:@"Order Date : %@",[_templateInfo objectForKey:@"order_date"]];
        
        if (NULL_TO_NIL([_templateInfo objectForKey:@"removal_reqdate"]))
            cell.lblOrderDate.text = [NSString stringWithFormat:@"Removal Req. Date : %@",[_templateInfo objectForKey:@"removal_reqdate"]];
        
        if (NULL_TO_NIL([_templateInfo objectForKey:@"totalqty"]))
            cell.lblQuantity.text = [NSString stringWithFormat:@"Quantity : %@",[_templateInfo objectForKey:@"totalqty"]];
        
        if (NULL_TO_NIL([_templateInfo objectForKey:@"order_no"]))
          cell.lblOrderNo.text = [NSString stringWithFormat:@"Order No : %@",[_templateInfo objectForKey:@"order_no"]];
        
        if (NULL_TO_NIL([_templateInfo objectForKey:@"totalprice"]))
            cell.lblSubTotal.text = [NSString stringWithFormat:@"Grand Total : $%.02f",[[_templateInfo objectForKey:@"totalprice"] floatValue]];
        
        if (NULL_TO_NIL([_templateInfo objectForKey:@"removal_fee"]))
            cell.lblSubTotal.text = [NSString stringWithFormat:@"Removal Fee : $%.02f",[[_templateInfo objectForKey:@"removal_fee"] floatValue]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;

    }else{
        
        static NSString *CellIdentifier = @"RemovalInfo";
        OrderDetailsTableViewCell *cell = (OrderDetailsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (NULL_TO_NIL([_templateInfo objectForKey:@"removal_template"]))
            cell.lblOrderNo.text = [NSString stringWithFormat:@"Removal Board Type : %@",[_templateInfo objectForKey:@"removal_template"]];
        
        if (NULL_TO_NIL([_templateInfo objectForKey:@"removal_price"]))
            cell.lblSubTotal.text = [NSString stringWithFormat:@"Removal Fee : $%.02f",[[_templateInfo objectForKey:@"removal_price"] floatValue]];
        
        if (NULL_TO_NIL([_templateInfo objectForKey:@"removal_text"]))
            cell.lblRmvalMsg.text = [_templateInfo objectForKey:@"removal_text"];
        
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        
        [self presentGalleryWithImages:arrImages andIndex:indexPath.row];
        
    }
}

- (CGFloat)tableView:(UITableView *)_tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        if(arrImages.count <= 0) return 200;
        CGFloat height = (_tableView.frame.size.width / 2.0)  ;
        CGFloat cellHeight = (numberOfGrids * height) ;
        return  cellHeight;
    }
    if (indexPath.section == 2) {
        
        return  UITableViewAutomaticDimension;
    }
    return kCellHeightFordetails;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return kHeightForHeader;
}


- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *vwHeader = [UIView new];
    vwHeader.backgroundColor = [UIColor getBackgroundOffWhiteColor];
    
    UILabel *lblTitle = [UILabel new];
    lblTitle.translatesAutoresizingMaskIntoConstraints = NO;
    [vwHeader addSubview:lblTitle];
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[lblTitle]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle)]];
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[lblTitle]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle)]];
   
    
    if (section == 0) {
        
        lblTitle.text = lblTitle.text = @"PREVIEW";
        
        UIButton* btnRetake = [UIButton new];
        btnRetake.backgroundColor = [UIColor getThemeColor];
        btnRetake.translatesAutoresizingMaskIntoConstraints = NO;
        [btnRetake setTitle:@"TAKE PHOTO" forState:UIControlStateNormal];
        [btnRetake setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnRetake.titleLabel.font = [UIFont fontWithName:CommonFont size:14];
        [btnRetake addTarget:self action:@selector(retakePhoto) forControlEvents:UIControlEventTouchUpInside];
        [vwHeader addSubview:btnRetake];
        
        [btnRetake addConstraint:[NSLayoutConstraint constraintWithItem:btnRetake
                                                             attribute:NSLayoutAttributeHeight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeHeight
                                                            multiplier:1.0
                                                              constant:40]];
        
        [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:btnRetake
                                                             attribute:NSLayoutAttributeTop
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:vwHeader
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1.0
                                                              constant:5]];
        
        [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:btnRetake
                                                             attribute:NSLayoutAttributeRight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:vwHeader
                                                             attribute:NSLayoutAttributeRight
                                                            multiplier:1.0
                                                              constant:-5]];

        
        [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:btnRetake
                                                             attribute:NSLayoutAttributeWidth
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeWidth
                                                            multiplier:1.0
                                                              constant:120]];

        
    }

    
    else if (section == 1) {
        lblTitle.text = @"ORDER DETAILS";
    } else if (section == 2) {
        lblTitle.text = @"ADD ON REMOVAL DETAILS";
    }
    
    
    lblTitle.font = [UIFont fontWithName:CommonFont size:16];
    lblTitle.textColor = [UIColor blackColor];
    
    return vwHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 001;
    
}

-(IBAction)removedSignBoard:(UIButton*)sender{
    
    if (isSignBoardRemoved) {
        [sender setImage:[UIImage imageNamed:@"Emotion_CheckBox"] forState:UIControlStateNormal];
        isSignBoardRemoved = false;
    }else{
         [sender setImage:[UIImage imageNamed:@"Emotion_CheckBox_Selected"] forState:UIControlStateNormal];
        isSignBoardRemoved = true;
    }
  
    
}
-(IBAction)deletePhoto:(UIButton*)sender{
    
    if (sender.tag < arrImages.count) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Delete"
                                                                       message:@"Do you really want to delete this image?"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"DELETE"
                                                              style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
                                                                  
                                                                  [arrImages removeObjectAtIndex:sender.tag];
                                                                  [self updateGalleryCell];
                                                                 
                                                                  
                                                              }];
        UIAlertAction *second = [UIAlertAction actionWithTitle:@"CANCEL"
                                                         style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                             
                                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                                         }];
        
        [alert addAction:firstAction];
        [alert addAction:second];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)updateGalleryCell{
    
    NSInteger items = arrImages.count;
    NSInteger count = (items / 2);
    if (arrImages.count % 2  > 0 ) {
        count ++;
    }
    
    numberOfGrids = count;
    [tableView reloadData];
}

-(void)retakePhoto{
    
    [self showCamera:nil];
}

-(IBAction)submitOrderStatus{
    
    if (!isSignBoardRemoved) {
        [[[UIAlertView alloc] initWithTitle:@"Removal Template" message:[_templateInfo objectForKey:@"removal_text"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        return;
    }
    
    
    NSString *orderID;
    if (NULL_TO_NIL( [_templateInfo objectForKey:@"order_id"]))
        orderID = [_templateInfo objectForKey:@"order_id"];
    
    if (orderID.length > 0) {
        if (!arrImages.count){
            [[[UIAlertView alloc] initWithTitle:@"Template Image" message:@"Please Pick an image for the Sign Post." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            return;
        }
    }
   
    if (arrImages.count) {
        [self showLoadingScreenWithTitle:@"Uploading.."];
        [self uploadImagesWithIndex:0];
    }
    
}


-(void)uploadImagesWithIndex:(NSInteger)index{
    
    __block NSInteger uploadIndex = index;
    
    if (index < arrImages.count) {
          [APIMapper uploadImagesByImage:arrImages[index] success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              if ([responseObject objectForKey:@"media_file"]) {
                  [arrUploadedNames addObject:[responseObject objectForKey:@"media_file"]];
              }
              uploadIndex ++;
              [self uploadImagesWithIndex:uploadIndex];
              
          } failure:^(AFHTTPRequestOperation *task, NSError *error) {
              
               [self hideLoadingScreen];
              
              if (error.localizedDescription) {
                   [[[UIAlertView alloc] initWithTitle:@"FINISH ORDER" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
              }
             
              [self resetUploadVariables];
          }];
    }else{
        
        [self uploadCompletedDetails];
    }
  
}

-(void)uploadCompletedDetails{
    
    
    [APIMapper completeOrderWithOrderID:[_templateInfo objectForKey:@"order_id"] images:arrUploadedNames isFinishOrder:_isRemovalOrder success:^(AFHTTPRequestOperation *operation, id responseObject) {
     
     if ([[responseObject objectForKey:@"code"] integerValue] == kSuccess) {
     
     NSString *title;
     if (NULL_TO_NIL([responseObject objectForKey:@"text"]))
     title = [responseObject objectForKey:@"text"];
     if (title.length > 0)
     [[[UIAlertView alloc] initWithTitle:@"Info" message:title delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
     [self.navigationController popToRootViewControllerAnimated:YES];
     
     }
     
     [self hideLoadingScreen];
     
     } failure:^(AFHTTPRequestOperation *task, NSError *error) {
     [self hideLoadingScreen];
     }];

}

#pragma mark - PhotoBrowser

- (void)presentGalleryWithImages:(NSArray*)images andIndex:(NSInteger)index
{
    
    if (!photoBrowser) {
        photoBrowser = [[[NSBundle mainBundle] loadNibNamed:@"PhotoBrowser" owner:self options:nil] objectAtIndex:0];
        photoBrowser.delegate = self;
    }
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *vwPopUP = photoBrowser;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser setUpWithImages:images andIndex:index];
    }];
    
}

-(void)closePhotoBrowserView{
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        photoBrowser.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser removeFromSuperview];
        photoBrowser = nil;
    }];
}



-(void)showLoadingScreenWithTitle:(NSString*)title{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = title;
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

-(void)resetUploadVariables{
    
    [arrUploadedNames removeAllObjects];
}

-(IBAction)goBack:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
