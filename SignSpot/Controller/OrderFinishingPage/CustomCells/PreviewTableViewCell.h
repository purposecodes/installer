//
//  PreviewTableViewCell.h
//  SignInstaller
//
//  Created by Purpose Code on 02/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreviewTableViewCell : UITableViewCell


@property (nonatomic,strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic,strong) IBOutlet NSLayoutConstraint *geight;

@end
