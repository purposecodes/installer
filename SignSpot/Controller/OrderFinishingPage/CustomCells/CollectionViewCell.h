//
//  CollectionViewCell.h
//  SignInstaller
//
//  Created by Purpose Code on 29/11/17.
//  Copyright © 2017 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell

@property (nonatomic,strong) IBOutlet UIImageView *imgPreview;
@property (nonatomic,strong) IBOutlet UIButton *btnCancel;

@end
