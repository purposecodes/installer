//
//  OrderDetailsTableViewCell.h
//  SignInstaller
//
//  Created by Purpose Code on 02/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailsTableViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel *lblOrderNo;
@property (nonatomic,strong) IBOutlet UILabel *lblQuantity;
@property (nonatomic,strong) IBOutlet UILabel *lblSubTotal;
@property (nonatomic,strong) IBOutlet UILabel *lblOrderDate;
@property (nonatomic,strong) IBOutlet UILabel *lblOrderDueDate;
@property (nonatomic,strong) IBOutlet UILabel *lblRmvalMsg;

@end
