//
//  QuickViewController.m
//  SignInstaller
//
//  Created by Purpose Code on 02/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "QuickViewController.h"
#import "Constants.h"
#import "DAMapAnnotationView.h"
#import "OrderDetailPageViewController.h"

@import GoogleMaps;

@interface QuickViewController () <GMSMapViewDelegate>{
    
    IBOutlet GMSMapView *_mapView;
    IBOutlet UISegmentedControl *segmentControl;
    NSMutableArray *arrPendingOrders;
    NSMutableArray *arrInstalledOrders;
    NSMutableArray *_coordinates;
    GMSPolyline *_polyline;
    GMSMarker *lstMarker;
    NSString *lstSelectedOrderTitle;
      IBOutlet UISegmentedControl *mapSegmentControll;
    // BOTTOM VIEW
    
    IBOutlet UIView *vwBottomSheet;
    IBOutlet UILabel *lblOrderNo;
    IBOutlet UILabel *lblQuantity;
    IBOutlet UILabel *lblEstDate;
    IBOutlet UILabel *lblOrderCount;
    IBOutlet UIImageView *imgTemplate;
    IBOutlet NSLayoutConstraint *botmContraint;
}

@end

@implementation QuickViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    //[self getCurrentUserLocation];
   
    // Do any additional setup after loading the view.
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)setUp{
    
    _mapView.mapType = kGMSTypeSatellite;
    _mapView.settings.rotateGestures = NO;
    _mapView.settings.tiltGestures = NO;
    
    mapSegmentControll.backgroundColor = [UIColor whiteColor];
    mapSegmentControll.layer.cornerRadius = 5;
    segmentControl.layer.cornerRadius = 5;
    
    arrPendingOrders = [NSMutableArray new];
    arrInstalledOrders = [NSMutableArray new];
    _coordinates = [NSMutableArray new];
    _mapView.delegate = self;
    [self getAllProductsByPagination:NO withPageNumber:1];
    
}


#pragma mark - Load & Place All Orders


-(void)getAllProductsByPagination:(BOOL)isPagination withPageNumber:(NSInteger)pageNumber{
    
    [self showLoadingScreen];
    [APIMapper getAllOrdersWithUserID:[User sharedManager].userId PageNumber:pageNumber isRemovalRequest:NO success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self getTemplatesFromResponds:responseObject];
        [self hideLoadingScreen];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        [self hideLoadingScreen];
        
    }];
    
    
}

-(void)getTemplatesFromResponds:(NSDictionary*)responseObject{
    
    if ( NULL_TO_NIL([responseObject objectForKey:@"installed"])) {
        NSArray *installed = [responseObject objectForKey:@"installed"];
        for (NSDictionary *dict in installed)
            [arrInstalledOrders addObject:dict];
    }
    if ( NULL_TO_NIL([responseObject objectForKey:@"pending"])) {
        NSArray *pending = [responseObject objectForKey:@"pending"];
        for (NSDictionary *dict in pending)
            [arrPendingOrders addObject:dict];
        
    }
    // Default Pending Orders
    
   [self showAllOrdersOnMapIsInstalled:NO];
    
}


-(void)showAllOrdersOnMapIsInstalled:(BOOL)isInstalledOrder{
    
    NSArray *orders;
    [_coordinates removeAllObjects];
    if (isInstalledOrder)
        orders = [NSArray arrayWithArray:arrInstalledOrders];
    else
        orders = [NSArray arrayWithArray:arrPendingOrders];
    lblOrderCount.text = [NSString stringWithFormat:@"You have %lu Order(s)",(unsigned long)orders.count];
    
    for (int i = 0; i < orders.count; i ++) {
       
        NSDictionary *loc = orders[i];
        
        if (NULL_TO_NIL([[loc objectForKey:@"map"] objectForKey:@"location"])) {
            
            NSArray *points = [[loc objectForKey:@"map"] objectForKey:@"location"];
            for (NSDictionary *dict in points) {
                
                float latitdue ;
                float longitude;
                
                if (dict && [dict objectForKey:@"location_latitude"])
                    latitdue = [[dict objectForKey:@"location_latitude"]floatValue];
                if (dict && [dict objectForKey:@"location_longitude"])
                    longitude = [[dict objectForKey:@"location_longitude"]floatValue];
                
                GMSMarker *marker = [[GMSMarker alloc] init];
                marker.appearAnimation = kGMSMarkerAnimationPop;
                marker.position =  CLLocationCoordinate2DMake(latitdue, longitude);
                NSDictionary *points = [[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithDouble:latitdue],@"latitude",[NSNumber numberWithDouble:longitude],@"longitude",nil];
               [_coordinates addObject:points];
                marker.map = _mapView;
                marker.tappable = YES;
                marker.userData = @{@"Title": [loc objectForKey:@"order_no"],
                                    @"Index": [NSNumber numberWithInteger:i]
                                    };
                [self getIconView:marker withText:[loc objectForKey:@"order_no"] isSelected:NO];
                marker.draggable = NO;
                _mapView.accessibilityElementsHidden = NO;
                marker.zIndex = 1;

            }
        }
    
    }
    [self focusMapToShowAllMarkers:_coordinates];

}

- (void)focusMapToShowAllMarkers:(NSMutableArray*)_markers;
{
    if (_markers.count <= 0 )  {
        [ALToastView toastInView:self.view withText:@"No Orders Available."];
        return;
    }
    NSDictionary *dict  = _markers[0];
    CLLocationDegrees latitude = [dict[@"latitude"] doubleValue];
    CLLocationDegrees longitude = [dict[@"longitude"] doubleValue];
    
    CLLocationCoordinate2D myLocation = CLLocationCoordinate2DMake(latitude, longitude);
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:myLocation coordinate:myLocation];
    
    for ( NSDictionary *dict  in _markers){
        
        CLLocationDegrees latitude = [dict[@"latitude"] doubleValue];
        CLLocationDegrees longitude = [dict[@"longitude"] doubleValue];
        CLLocationCoordinate2D coordinates = CLLocationCoordinate2DMake(latitude, longitude);
        bounds = [bounds includingCoordinate:coordinates];
    }
    
    [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:100.0f]];
}




#pragma mark - Map Deleagte

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    [self hideBottomSheet];
}

-(BOOL) mapView:(GMSMapView *) mapView didTapMarker:(GMSMarker *)marker
{
    NSDictionary *markerInfo = marker.userData;
    NSInteger tag = [[markerInfo objectForKey:@"Index"] integerValue];
    NSString *title = [markerInfo objectForKey:@"Title"];
    if (lstMarker)[self getIconView:lstMarker withText:lstSelectedOrderTitle isSelected:NO];
    [self getIconView:marker withText:title isSelected:YES];
    [self showBottomSheetWithTag:tag];
    lstMarker = marker;
    lstSelectedOrderTitle = title;
     return NO;
}



-(void)getIconView:(GMSMarker*)marker withText:(NSString*)title isSelected:(BOOL)isSelected{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,100,50)];
     UIImageView *pinImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bubble_left"]];
    if (isSelected) {
         pinImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bubble_right"]];
    }
    UILabel *label = [UILabel new];
    label.frame = CGRectMake(0, 0, 100, 50);
    label.text = title;
    label.textAlignment = NSTextAlignmentCenter;
    [label setFont:[UIFont fontWithName:CommonFont size:14]];
    [label setTextColor: [UIColor whiteColor]];
    [view addSubview:pinImageView];
    [view addSubview:label];
    UIImage *markerIcon = [self imageFromView:view];
    marker.icon = markerIcon;
}

- (UIImage *)imageFromView:(UIView *) view
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(view.frame.size);
    }
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

#pragma mark - User Actions and Generic Methods

-(IBAction)changeMapView{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // do work here
        
        if ([mapSegmentControll selectedSegmentIndex] == 0) _mapView.mapType =  kGMSTypeSatellite;
        else _mapView.mapType =  kGMSTypeNormal;
         [_mapView reloadInputViews];
    });
    
    
    
    
}

-(IBAction)switchOrderType:(UISegmentedControl*)sender{
    
    [_mapView clear];
    [self hideBottomSheet];
    
    if ([sender selectedSegmentIndex] == 0) {
        [self showAllOrdersOnMapIsInstalled:NO];
    }else{
        [self showAllOrdersOnMapIsInstalled:YES];
    }
}



-(void)showBottomSheetWithTag:(NSInteger)tag{
    
    if ([segmentControl selectedSegmentIndex] == 0) {
        
        if (tag < arrPendingOrders.count) {
            NSDictionary *_details = arrPendingOrders[tag];
            if (NULL_TO_NIL([_details objectForKey:@"order_duedate"]))
                lblEstDate.text = [NSString stringWithFormat:@"Est.Installation Date : %@",[_details objectForKey:@"order_duedate"]];
            if (NULL_TO_NIL([_details objectForKey:@"order_no"]))
                lblOrderNo.text = [NSString stringWithFormat:@"Order No : %@",[_details objectForKey:@"order_no"]];
            if (NULL_TO_NIL([_details objectForKey:@"totalqty"]))
                lblQuantity.text = [NSString stringWithFormat:@"Quantity : %@",[_details objectForKey:@"totalqty"]];

            if ([_details objectForKey:@"sign_image"] && [[_details objectForKey:@"sign_image"] length]){
                [imgTemplate sd_setImageWithURL:[NSURL URLWithString:[_details objectForKey:@"sign_image"]]
                               placeholderImage:[UIImage imageNamed:@"NoImage.png"]
                                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                      }];
            }

        }
        
    }else{
        
        if (tag < arrInstalledOrders.count) {
            
            NSDictionary *_details = arrInstalledOrders[tag];
            if (NULL_TO_NIL([_details objectForKey:@"order_duedate"]))
                lblEstDate.text = [NSString stringWithFormat:@"Installed Date : %@",[_details objectForKey:@"order_duedate"]];
            if (NULL_TO_NIL([_details objectForKey:@"order_no"]))
                lblOrderNo.text = [NSString stringWithFormat:@"Order No : %@",[_details objectForKey:@"order_no"]];
            if (NULL_TO_NIL([_details objectForKey:@"totalqty"]))
                lblQuantity.text = [NSString stringWithFormat:@"Quantity : %@",[_details objectForKey:@"totalqty"]];
            
            if ([_details objectForKey:@"sign_image"] && [[_details objectForKey:@"sign_image"] length]){
                [imgTemplate sd_setImageWithURL:[NSURL URLWithString:[_details objectForKey:@"sign_image"]]
                               placeholderImage:[UIImage imageNamed:@"NoImage.png"]
                                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                      }];
                
            }
            
            
        }

        
        
    }
    
    botmContraint.constant = 0;
    [UIView animateWithDuration:.5 animations:^{
    [self.view layoutIfNeeded];
    }];
}
-(void)hideBottomSheet{
    
    CGFloat bottomPadding = 0;
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        bottomPadding = window.safeAreaInsets.bottom;
        
    }
    
    botmContraint.constant = - 90 - bottomPadding;
    [UIView animateWithDuration:.5 animations:^{
        [self.view layoutIfNeeded];
    }];
}

-(IBAction)showDetailPage:(id)sender{
    
    NSDictionary *markerInfo = lstMarker.userData;
    NSInteger tag = [[markerInfo objectForKey:@"Index"] integerValue];
    NSDictionary *details;
    OrderDetailPageViewController *mapDirectionsPage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:MenuDetailsStoryBoard Identifier:StoryBoardIdentifierForMapPage];
    if ([segmentControl selectedSegmentIndex] == 0) {
        if (tag < arrPendingOrders.count)
            details = arrPendingOrders[tag];
        mapDirectionsPage.isPending = true;
    }else{
        
        if (tag < arrInstalledOrders.count)
            details = arrInstalledOrders[tag];
        mapDirectionsPage.isPending = false;
        
    }
    if (details) {
        mapDirectionsPage.isRemovalOrder = false;
        mapDirectionsPage.dictLocationInfo = details;
        [self.navigationController pushViewController:mapDirectionsPage animated:YES];
        
    }
   
}

-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    
}


-(IBAction)goBack:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
