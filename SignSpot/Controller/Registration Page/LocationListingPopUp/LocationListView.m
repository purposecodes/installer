//
//  CountryListView.m
//  SignSpot
//
//  Created by Purpose Code on 10/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//


#define kCellHeight   40;
#define kHeightForHeader 0;


#import "LocationListView.h"
#import "Constants.h"

@interface LocationListView()<UISearchBarDelegate>{
    
    UISearchBar *searchBar;
    NSMutableArray *arrFiltered;
}

@end

@implementation LocationListView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setUp{
    
    [self loadUI];
    
    
}

-(void)loadUI{
    
    
    arrFiltered = [NSMutableArray new];
    self.backgroundColor = [UIColor colorWithWhite:0 alpha:.5];
    arrCountries = [NSMutableArray new];
    
    // Tableview Setup
    
    tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    tableView.translatesAutoresizingMaskIntoConstraints = NO;
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self addSubview:tableView];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-80-[tableView]-40-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tableView)]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-30-[tableView]-30-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tableView)]];
    tableView.delegate = self;
    tableView.dataSource = self;
    
    
    // Tap Gesture SetUp
    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closePopUp)];
    gesture.numberOfTapsRequired = 1;
    gesture.delegate = self;
    [self addGestureRecognizer:gesture];
}

-(void)setUpHeaderSearchView{
    
    UIView *vwHeader = [UIView new];
    [self addSubview:vwHeader];
    vwHeader.translatesAutoresizingMaskIntoConstraints = NO;
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-30-[vwHeader]-30-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwHeader)]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:vwHeader
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:nil
                                                     attribute:NSLayoutAttributeNotAnAttribute 
                                                    multiplier:1.0 
                                                      constant:40]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:vwHeader
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeTop
                                                    multiplier:1.0
                                                      constant:40]];
    
    vwHeader.backgroundColor = [UIColor whiteColor];
    
    searchBar = [UISearchBar new];
    searchBar.translatesAutoresizingMaskIntoConstraints = NO;
    [vwHeader addSubview:searchBar];
    searchBar.delegate = self;
    searchBar.enablesReturnKeyAutomatically = NO;
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[searchBar]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(searchBar)]];
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[searchBar]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(searchBar)]];
    if (locType == eStateList) searchBar.placeholder = @"Search Your State";
    if (locType == eCityList)  searchBar.placeholder = @"Search Your City";
}

-(void)loadLocationsOn:(eLocationList)typeOfLocation{
    
    switch (typeOfLocation) {
            
        case eStateList:
            [self loadAllStates];
            break;
            
        case eCityList:
            [self loadAllCities];
            break;
            
        default:
            break;
    }
}


-(void)loadAllStates{
    
    locType = eStateList;
   
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        // Add code here to do background processing
        //
        
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"CountryList" ofType:@"txt"];
        NSString *countryJSON = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
        NSData *jsonData = [countryJSON dataUsingEncoding:NSUTF8StringEncoding];
        NSError *e;
        NSDictionary *country = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&e];
        
        if ( NULL_TO_NIL([country objectForKey:@"state"])) {
            NSArray *result = [country objectForKey:@"state"];
            for (NSDictionary *dict in result) {
                [arrCountries addObject:dict];
            }
            
             arrFiltered = [NSMutableArray arrayWithArray:arrCountries];
        }
        dispatch_async( dispatch_get_main_queue(), ^{
            if ([arrFiltered count]) {
                isDataAvailable = true;
            }
            [self setUpHeaderSearchView];
            [tableView reloadData];
        });
    });

    
}

-(void)loadAllCities{
    
     locType = eCityList;
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        // Add code here to do background processing
        //
        
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"CountryList" ofType:@"txt"];
        NSString *countryJSON = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
        NSData *jsonData = [countryJSON dataUsingEncoding:NSUTF8StringEncoding];
        NSError *e;
        NSDictionary *country = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&e];
        
        if ( NULL_TO_NIL([country objectForKey:@"state"])) {
            NSArray *result = [country objectForKey:@"state"];
            for (NSDictionary *dict in result) {
                if ([[dict objectForKey:@"id"] isEqualToString:_selectedID]) {
                    NSArray *city = [dict objectForKey:@"city"];
                    arrCountries = [NSMutableArray arrayWithArray:city];
                    break;
                }
            }
            arrFiltered = [NSMutableArray arrayWithArray:arrCountries];
        }
        dispatch_async( dispatch_get_main_queue(), ^{
            if ([arrFiltered count]) {
                isDataAvailable = true;
            }
            [self setUpHeaderSearchView];
            [tableView reloadData];
        });
    });
}


#pragma mark - TableView Delegates


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(!isDataAvailable) return 1;
    return [arrFiltered count];    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!isDataAvailable) {
         UITableViewCell *cell = [Utility getNoDataCustomCellWith:_tableView withTitle:@"No Listing found."];
        return cell;
    }
    
    static NSString *MyIdentifier = @"Identifier";
    UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                          reuseIdentifier:MyIdentifier];
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.imageView.image = [UIImage imageNamed:@"UnCheckMark.png"];
    if (indexPath.row < arrFiltered.count) {
        
        NSDictionary *country = [arrFiltered objectAtIndex:indexPath.row];
        if ( NULL_TO_NIL( [country objectForKey:@"name"])) {
             cell.textLabel.text = [country objectForKey:@"name"];
        }
        if (_selectedID == [country objectForKey:@"id"]) cell.imageView.image = [UIImage imageNamed:@"CheckMark.png"];
        
    }
   
    cell.textLabel.font = [UIFont fontWithName:CommonFont size:15];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return kCellHeight;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return kHeightForHeader;
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row < arrFiltered.count) {
        NSDictionary *country = [arrFiltered objectAtIndex:indexPath.row];
        if (country)
            _selectedID = [country objectForKey:@"id"];
            [[self delegate]sendSelectedLocationsWith:country type:locType];
    }
    [tableView reloadData];
    float delay = .2;
    [[self delegate]closeLocationListingPopUpAfterADelay:delay];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    [searchBar resignFirstResponder];
    
}
#pragma mark - Search Methods and Delegates


- (void)searchBar:(UISearchBar *)_searchBar textDidChange:(NSString *)searchString{
    [arrFiltered removeAllObjects];
    if (searchString.length > 0) {
        if (arrCountries.count > 0) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSPredicate *predicate =[[NSPredicate predicateWithFormat:@"(name CONTAINS[cd] $str)"] predicateWithSubstitutionVariables:@{@"str": searchString}];
                arrFiltered = [NSMutableArray arrayWithArray:[arrCountries filteredArrayUsingPredicate:predicate]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [tableView reloadData];
                });
            });
            
        }
    }else{
        [searchBar resignFirstResponder];
        arrFiltered = [NSMutableArray arrayWithArray:arrCountries];
        [tableView reloadData];
    }
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)_searchBar{
    
    [_searchBar resignFirstResponder];
    
}



#pragma mark - Common Methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:tableView])
        return NO;
    return YES;
}


-(IBAction)closePopUp{
    
    [[self delegate]closeLocationListingPopUpAfterADelay:0];
}

-(void)showLoadingScreen{
    
    AppDelegate *deleagte = (AppDelegate*)[UIApplication sharedApplication].delegate;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:deleagte.window animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    AppDelegate *deleagte = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [MBProgressHUD hideHUDForView:deleagte.window animated:YES];
    
}

@end
