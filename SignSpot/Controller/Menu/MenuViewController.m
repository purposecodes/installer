//
//  MenuViewController.m
//  RevealControllerStoryboardExample
//
//  Created by Nick Hodapp on 1/9/13.
//  Copyright (c) 2013 CoDeveloper. All rights reserved.
//

#define kTagForTitle     1
#define kCellHeight         40
#define kHeightForHeader    60

NSString *const HomeMenu        = @"HOME";
NSString *const UserTemplate    = @"USER TEMPLATE";

#import "MenuViewController.h"
#import "HomeViewController.h"
#import "Constants.h"

@implementation SWUITableViewCell
@end

@implementation MenuViewController{
    
    NSMutableArray *arrCategories;
}


- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    // configure the destination view controller:
    if ( [sender isKindOfClass:[UITableViewCell class]] ){
    }
}
-(void)viewDidLoad{
    
    [super viewDidLoad];
    [self setUp];
    [self loadAllCategories];
}

-(void)setUp{
    
    arrCategories = [NSMutableArray new];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
   // self.view.backgroundColor = [UIColor colorWithRed:51.f/255.f green:45.f/255.f blue:64.f/255.f alpha:1];
   // self.tableView.backgroundColor = [UIColor whiteColor];
    
}

-(void)loadAllCategories{
    
    [APIMapper getAllCategoriesOnsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self getAllCategoriesWith:responseObject];
        
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        
        
    }];
}

-(void)getAllCategoriesWith:(NSDictionary*)category{
 
    if (  NULL_TO_NIL([category objectForKey:@"resultarray"])) {
        [arrCategories removeAllObjects];
        // Add Item "Home" to category list as First Item
        NSDictionary *home = [NSDictionary dictionaryWithObject:HomeMenu forKey:@"category_title"];
        [arrCategories addObject:home];
        NSArray *categories = [category objectForKey:@"resultarray"];
        for (NSDictionary *dict in categories)[arrCategories addObject:dict];
        NSDictionary *userTemplate = [NSDictionary dictionaryWithObject:UserTemplate forKey:@"category_title"];
        [arrCategories addObject:userTemplate];
        [self.tableView reloadData];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrCategories.count;
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    
    UITableViewCell *cell = (UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell)cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    NSString *title;
    if (indexPath.row < arrCategories.count) {
        title = [arrCategories[indexPath.row] objectForKey:@"category_title"];
    }
    if ([[cell contentView]viewWithTag:kTagForTitle]) {
        UILabel *lblTitle = (UILabel*)[[cell contentView]viewWithTag:kTagForTitle];
        lblTitle.text = title;
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kCellHeight;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIFont *font = [UIFont fontWithName:CommonFont size:13];
    
    UIView *vwHeader = [UIView new];
    vwHeader.backgroundColor = [UIColor colorWithRed:51.f/255.f green:45.f/255.f blue:64.f/255.f alpha:1];
    
    //User Profile Pic
    
    UIImageView *imgDisplay = [UIImageView new];
    [vwHeader addSubview:imgDisplay];
    imgDisplay.translatesAutoresizingMaskIntoConstraints = NO;
    imgDisplay.clipsToBounds = YES;
    imgDisplay.layer.cornerRadius = 20.f;
    imgDisplay.layer.borderWidth = 3.f;
    imgDisplay.backgroundColor = [UIColor whiteColor];
    imgDisplay.layer.borderColor = [UIColor grayColor].CGColor;
    imgDisplay.contentMode = UIViewContentModeScaleAspectFit;
    [imgDisplay addConstraint:[NSLayoutConstraint constraintWithItem:imgDisplay
                                                           attribute:NSLayoutAttributeWidth
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeWidth
                                                          multiplier:1.0
                                                            constant:40.0]];
    [imgDisplay addConstraint:[NSLayoutConstraint constraintWithItem:imgDisplay
                                                           attribute:NSLayoutAttributeHeight
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeHeight
                                                          multiplier:1.0
                                                            constant:40.0]];
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:imgDisplay
                                                         attribute:NSLayoutAttributeLeft
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:vwHeader
                                                         attribute:NSLayoutAttributeLeft
                                                        multiplier:1.0
                                                          constant:10.0]];
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:imgDisplay
                                                           attribute:NSLayoutAttributeCenterY
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:vwHeader
                                                           attribute:NSLayoutAttributeCenterY
                                                          multiplier:1.0
                                                            constant:0.0]];
    
    if ([User sharedManager].profileurl.length) {
        
        [imgDisplay sd_setImageWithURL:[NSURL URLWithString:[User sharedManager].profileurl]
                      placeholderImage:[UIImage imageNamed:@"UserProfilePic.png"]
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                 
                             }];
    }

    /*! User Name !*/
  
    UILabel *lblName = [UILabel new];
    lblName.translatesAutoresizingMaskIntoConstraints = NO;
    [vwHeader addSubview:lblName];
    lblName.numberOfLines = 1;
    lblName.textColor = [UIColor whiteColor];
    lblName.font = font;
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:lblName
                                                         attribute:NSLayoutAttributeTop
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:imgDisplay
                                                         attribute:NSLayoutAttributeTop
                                                        multiplier:1.0
                                                          constant:7.0]];
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-60-[lblName]-70-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblName)]];
        lblName.text = [User sharedManager].name;
    
    /*! User Other Details !*/
    
    UILabel *lblOtherInfo = [UILabel new];
    lblOtherInfo.translatesAutoresizingMaskIntoConstraints = NO;
    [vwHeader addSubview:lblOtherInfo];
    lblOtherInfo.numberOfLines = 1;
    lblOtherInfo.font = font;
    lblOtherInfo.textColor = [UIColor lightGrayColor];
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:lblOtherInfo
                                                         attribute:NSLayoutAttributeTop
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:lblName
                                                         attribute:NSLayoutAttributeBottom
                                                        multiplier:1.0
                                                          constant:0.0]];
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-60-[lblOtherInfo]-70-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblOtherInfo)]];
    lblOtherInfo.text = [User sharedManager].email;
   
    return vwHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return kHeightForHeader;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row < arrCategories.count) {
        
        /**! Get RootVC from the navigation controller.Since SWReavealcontroller is initialised by two NAV Controller on Front and Rear.!**/
        
        UINavigationController *navController = (UINavigationController*)self.revealViewController.frontViewController;
        NSArray *viewControllers = navController.viewControllers;
        if (viewControllers.count) {
            HomeViewController *homeVC = viewControllers[0];
                   }
    }
    [self.revealViewController revealToggleAnimated:YES];
  
}


#pragma mark state preservation / restoration
- (void)encodeRestorableStateWithCoder:(NSCoder *)coder {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // TODO save what you need here
    
    [super encodeRestorableStateWithCoder:coder];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // TODO restore what you need here
    
    [super decodeRestorableStateWithCoder:coder];
}

- (void)applicationFinishedRestoringState {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // TODO call whatever function you need to visually restore
}

@end
