//
//  ProductCollectionViewCell.h
//  SignSpot
//
//  Created by Purpose Code on 12/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"

@protocol ProductCellDelegate <NSObject>


@optional
/*!
 *This method is invoked when user taps the 'Close' Button.
 */

-(void)getSelectedProductFromPosition:(NSInteger)row column:(NSInteger)column;
-(void)cartButtonAppliedFromPoistion:(NSInteger)row column:(NSInteger)column;
-(void)shareButtonAppliedFromPoistion:(NSInteger)row column:(NSInteger)column;


/*!
 *This method is invoked when user selects a country.The selected Country Details sends back to Registration page
 */

@end



@interface OrderListCollectionViewCell : UICollectionViewCell{
    
}

@property (nonatomic, assign) NSInteger section;
@property (nonatomic, assign) NSInteger row;
@property (nonatomic,weak)  id<ProductCellDelegate>delegate;

@property (nonatomic,weak) IBOutlet UIImageView *templateImage;
@property (nonatomic,weak) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, weak) IBOutlet UILabel *lblOrderNo;
@property (nonatomic, weak) IBOutlet UILabel *lblOrderDate;
@property (nonatomic, weak) IBOutlet UILabel *lblOrderDueDate;
@property (nonatomic, weak) IBOutlet UILabel *lblOrderPrice;
@property (nonatomic, weak) IBOutlet UILabel *lblQuantity;
@property (nonatomic, weak) IBOutlet UILabel *lblAddress;
@property (nonatomic,weak) IBOutlet UIView *vwBG;

-(void)setUpIndexPathWithRow:(NSInteger)row section:(NSInteger)section;
-(void)setupShadow;


@end
