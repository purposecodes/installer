//
//  ProductCollectionViewCell.m
//  SignSpot
//
//  Created by Purpose Code on 12/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "OrderListCollectionViewCell.h"


@implementation OrderListCollectionViewCell

-(void)setUpIndexPathWithRow:(NSInteger)row section:(NSInteger)section;{
    
    self.row = row;
    self.section = section;
}

-(void)setupShadow{
    
    [_vwBG.layer setBorderColor:[UIColor clearColor].CGColor];
    [_vwBG.layer setBorderWidth:1.f];
    // drop shadow
    [_vwBG.layer setShadowColor:[UIColor blackColor].CGColor];
    [_vwBG.layer setShadowOpacity:0.3];
    [_vwBG.layer setShadowRadius:2.0];
    [_vwBG.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
}


@end
