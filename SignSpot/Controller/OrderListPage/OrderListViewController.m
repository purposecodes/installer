//
//  TemplateListViewController.m
//  SignSpot
//
//  Created by Purpose Code on 13/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

static NSString *CollectionViewCellIdentifier = @"OrderListCollectionViewCell";

typedef enum{
    
    ePending = 0,
    eCompleted = 1,
    
    
}eOrderType;


#define kPadding                10
#define kDefaultNumberOfCells   1
#define kHeightForHeader        50

#import "OrderListViewController.h"
#import "OrderListCollectionViewCell.h"
#import "OrderListCollectionViewHeader.h"
#import "OrderDetailPageViewController.h"
#import "Constants.h"

@interface OrderListViewController (){
    
    IBOutlet UICollectionView *collectionView;
    IBOutlet UIButton *btnViewType;
    IBOutlet UIView *vwSearch;
    IBOutlet UIView *vwTitle;
     IBOutlet UILabel *lblTitle;
    IBOutlet UISearchBar *searchBar;
    IBOutlet UIView *vwPaginationPopUp;
    IBOutlet NSLayoutConstraint *paginationBottomConstraint;
    NSMutableArray *arrPendingOrders;
    NSMutableArray *arrInstalledOrders;
    NSMutableArray *arrDataSource;
    NSMutableArray *arrFiltered;
    NSInteger totalPages;
    NSInteger currentPage;
    
    BOOL isPageRefresing;
    BOOL isDataAvailable;
    BOOL isListType;
    UIRefreshControl *refreshControl;
    
    IBOutlet UISegmentedControl *segmentControll;
    
}


@end

@implementation OrderListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    [self getAllProductsByPagination:NO withPageNumber:currentPage];
    
    
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)setUp{
    
    lblTitle.text = @"ORDERS";
    if (_isRemovalOrder) {
         lblTitle.text = @"REMOVAL ORDERS";
    }
    [btnViewType setImage:[UIImage imageNamed:@"Grid"] forState:UIControlStateNormal];
    UINib *cellNib = [UINib nibWithNibName:@"OrderListCollectionViewCell" bundle:nil];
    [collectionView registerNib:cellNib forCellWithReuseIdentifier:CollectionViewCellIdentifier];
    collectionView.hidden = false;
    
    arrInstalledOrders = [NSMutableArray new];
    arrPendingOrders =  [NSMutableArray new];
    arrFiltered = [NSMutableArray new];
    currentPage = 1;
    totalPages = 0;
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    [collectionView addSubview:refreshControl];
    collectionView.alwaysBounceVertical = YES;
    collectionView.hidden = true;
    isDataAvailable = false;
    segmentControll.layer.cornerRadius = 5.f;
    
    searchBar.layer.borderWidth = 1;
    searchBar.layer.borderColor = [UIColor getThemeColor].CGColor;
    searchBar.enablesReturnKeyAutomatically = NO;
    isListType = true;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self.view bringSubviewToFront:vwPaginationPopUp];
}

-(IBAction)changeListType:(id)sender{
    
    [btnViewType setImage:[UIImage imageNamed:@"List"] forState:UIControlStateNormal];
    isListType = !isListType;
    if (isListType) [btnViewType setImage:[UIImage imageNamed:@"Grid"] forState:UIControlStateNormal];
    [collectionView reloadData];
    
}



#pragma mark - Methods for  Getting All Templates by Pagination as well as Intial load.


-(void)getAllProductsByPagination:(BOOL)isPagination withPageNumber:(NSInteger)pageNumber{
    
    if (!isPagination) {
          [self showLoadingScreen];
    }
    
    
    [APIMapper getAllOrdersWithUserID:[User sharedManager].userId PageNumber:pageNumber isRemovalRequest:_isRemovalOrder success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        collectionView.hidden = false;
        isPageRefresing = NO;
        [self getTemplatesFromResponds:responseObject];
        [collectionView reloadData];
        [self hideLoadingScreen];
        [self hidePaginationPopUp];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        collectionView.hidden = false;
        isDataAvailable = false;
        isPageRefresing = NO;
        [self hideLoadingScreen];
        [self hidePaginationPopUp];
        [collectionView reloadData];
        
    }];
    

}

-(void)getTemplatesFromResponds:(NSDictionary*)responseObject{
    
    isDataAvailable = false;
    
    if (_isRemovalOrder) {
        // If coming from Removal Tab Click
        if ( NULL_TO_NIL([responseObject objectForKey:@"removed"])) {
            NSArray *installed = [responseObject objectForKey:@"removed"];
            for (NSDictionary *dict in installed)
                [arrInstalledOrders addObject:dict];
        }
    }else{
        if ( NULL_TO_NIL([responseObject objectForKey:@"installed"])) {
            NSArray *installed = [responseObject objectForKey:@"installed"];
            for (NSDictionary *dict in installed)
                [arrInstalledOrders addObject:dict];
        }
    }
    
    if ( NULL_TO_NIL([responseObject objectForKey:@"pending"])) {
        NSArray *pending = [responseObject objectForKey:@"pending"];
        for (NSDictionary *dict in pending)
            [arrPendingOrders addObject:dict];
    }
    
    if (NULL_TO_NIL([responseObject objectForKey:@"pageCount"]))
         totalPages =  [[responseObject objectForKey:@"pageCount"]integerValue];

    if (NULL_TO_NIL([responseObject objectForKey:@"currentPage"]))
        currentPage =  [[responseObject objectForKey:@"currentPage"]integerValue];
    
    if ([segmentControll selectedSegmentIndex] == ePending) {
        arrDataSource = [NSMutableArray arrayWithArray:arrPendingOrders];
    }else if ([segmentControll selectedSegmentIndex] == eCompleted) {
         arrDataSource = [NSMutableArray arrayWithArray:arrInstalledOrders];
    }
    arrFiltered = [NSMutableArray arrayWithArray:arrDataSource];
    if (arrFiltered.count > 0)
        isDataAvailable = true;
    
    if (!isDataAvailable){
         [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
    }else{
        
        UINib *cellNib = [UINib nibWithNibName:@"OrderListCollectionViewCell" bundle:nil];
        [collectionView registerNib:cellNib forCellWithReuseIdentifier:CollectionViewCellIdentifier];

    }

}



-(void)refreshData{
    
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    if (isPageRefresing){
        [refreshControl endRefreshing];
        return;
    }
    [self showLoadingScreen];
    currentPage = 1;
    isPageRefresing = YES;
    
    [APIMapper getAllOrdersWithUserID:[User sharedManager].userId PageNumber:currentPage isRemovalRequest:_isRemovalOrder success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        isPageRefresing = NO;
        [arrPendingOrders removeAllObjects];
        [arrInstalledOrders removeAllObjects];
        [self getTemplatesFromResponds:responseObject];
        [collectionView reloadData];
        [refreshControl endRefreshing];
        [self hideLoadingScreen];

        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        isPageRefresing = NO;
        isDataAvailable = false;
        [refreshControl endRefreshing];
        [collectionView reloadData];
        [self hideLoadingScreen];
        
    }];
    
 

}

-(IBAction)switchOrderType:(UISegmentedControl*)sender{
    
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    [self checkAvailabilityOfTemplates];
    [collectionView reloadData];
    if (arrFiltered.count)
        [collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                               atScrollPosition:UICollectionViewScrollPositionTop
                                        animated:YES];
}

-(void)checkAvailabilityOfTemplates{
    
    isDataAvailable = false;
    
    if ([segmentControll selectedSegmentIndex] == ePending) {
        arrDataSource = [NSMutableArray arrayWithArray:arrPendingOrders];
    }else if ([segmentControll selectedSegmentIndex] == eCompleted) {
        arrDataSource = [NSMutableArray arrayWithArray:arrInstalledOrders];
    }
    arrFiltered = [NSMutableArray arrayWithArray:arrDataSource];
    if (arrFiltered.count > 0) isDataAvailable = true;
    

}

#pragma mark - Change title and search visibility

-(IBAction)showSearchbar{
    
    vwSearch.hidden = false;
    vwTitle.hidden = true;
}
-(IBAction)showTitleView{
    
    vwSearch.hidden = true;
    vwTitle.hidden = false;
}
  
#pragma mark - Search Methods and Delegates


- (void)searchBar:(UISearchBar *)_searchBar textDidChange:(NSString *)searchString{
    
    [arrFiltered removeAllObjects];
    if (searchString.length > 0) {
        if (arrDataSource.count > 0) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSPredicate *predicate =[[NSPredicate predicateWithFormat:@"(order_no CONTAINS[cd] $str)"] predicateWithSubstitutionVariables:@{@"str": searchString}];
                arrFiltered = [NSMutableArray arrayWithArray:[arrDataSource filteredArrayUsingPredicate:predicate]];
            });
            
        }
    }else{
         [searchBar resignFirstResponder];
         arrFiltered = [NSMutableArray arrayWithArray:arrDataSource];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [collectionView reloadData];
    });
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)_searchBar{
    
    [_searchBar resignFirstResponder];
    
}

#pragma mark - UICollectionViewDataSource Methods

-(NSInteger)collectionView:(UICollectionView *)_collectionView numberOfItemsInSection:(NSInteger)section
{
    if (!isDataAvailable) return kDefaultNumberOfCells;
    return arrFiltered.count;
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)_collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (!isDataAvailable) {
        UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        return cell;
    }
    OrderListCollectionViewCell *cell = [_collectionView dequeueReusableCellWithReuseIdentifier:CollectionViewCellIdentifier forIndexPath:indexPath];
    cell.layer.borderWidth = 1.f;
    cell.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    if (isListType) {
        cell = [_collectionView dequeueReusableCellWithReuseIdentifier:@"ListView" forIndexPath:indexPath];
        cell.layer.borderWidth = 0;
        [cell setupShadow];
    }
    
    [cell setUpIndexPathWithRow:indexPath.row section:indexPath.section];
    NSDictionary *template;
    
    if (indexPath.row < arrFiltered.count) {
        
        template = arrFiltered[indexPath.row];
        
        if (NULL_TO_NIL([template objectForKey:@"order_duedate"]))
            cell.lblOrderDueDate.text = [NSString stringWithFormat:@"Due Date : %@",[template objectForKey:@"order_duedate"]];
        
        if (NULL_TO_NIL([template objectForKey:@"removal_duedate"]))
            cell.lblOrderDueDate.text = [NSString stringWithFormat:@"Due Date : %@",[template objectForKey:@"removal_duedate"]];
        
        if (NULL_TO_NIL([template objectForKey:@"order_no"]))
            cell.lblOrderNo.text = [NSString stringWithFormat:@"Order No : %@",[template objectForKey:@"order_no"]];
        
        if (NULL_TO_NIL([template objectForKey:@"totalqty"]))
            cell.lblQuantity.text = [NSString stringWithFormat:@"Quantity : %@",[template objectForKey:@"totalqty"]];
        
        if (NULL_TO_NIL([template objectForKey:@"location_address"])){
            
            NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc] initWithString:[template objectForKey:@"location_address"]];
            NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
            [style setLineSpacing:1];
            [attrString addAttribute:NSParagraphStyleAttributeName
                               value:style
                               range:NSMakeRange(0, attrString.length)];
            cell.lblAddress.attributedText = attrString;
            
        }
        
        if ([template objectForKey:@"sign_image"] && [[template objectForKey:@"sign_image"] length]){
            [cell.activityIndicator startAnimating];
            [cell.templateImage sd_setImageWithURL:[NSURL URLWithString:[template objectForKey:@"sign_image"]]
                                  placeholderImage:[UIImage imageNamed:@"NoImage.png"]
                                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                             [cell.activityIndicator stopAnimating];
                                         }];
            
        }

    }
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)_collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    float percentage = 80;
    float width = _collectionView.bounds.size.width - 10;
    float delta = 0;
    float padding = 0;
    float height = ((width - padding) * percentage) / 100 + delta;
    if (isListType) {
        height = 100;
    }
    return CGSizeMake(width, height);
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)_collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        OrderListCollectionViewHeader *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"OrderListCollectionViewHeader" forIndexPath:indexPath];
         headerView.lblCategoryTitle.text = [NSString stringWithFormat:@"You have %lu Order(s)",(unsigned long)arrFiltered.count];
        reusableview = headerView;
    }
    
    
    return reusableview;
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    /**! Pagination call !**/
    
    
    if(collectionView.contentOffset.y >= (collectionView.contentSize.height - collectionView.bounds.size.height)) {
       
        if(isPageRefresing == NO){ // no need to worry about threads because this is always on main thread.
          
            NSInteger nextPage = currentPage ;
            nextPage += 1;
            if (nextPage  <= totalPages) {
                isPageRefresing = YES;
                [self showPaginationPopUp];
                [self getAllProductsByPagination:YES withPageNumber:nextPage];
            }
        
        }
    }
    
}


- (CGSize)collectionView:(UICollectionView *)_collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    if (!isDataAvailable) return CGSizeZero;
    return CGSizeMake(_collectionView.bounds.size.width, 45);
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    OrderDetailPageViewController *mapDirectionsPage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:MenuDetailsStoryBoard Identifier:StoryBoardIdentifierForMapPage];
    if (indexPath.row < arrFiltered.count)
        mapDirectionsPage.dictLocationInfo = arrFiltered[[indexPath row]];
    
    if ([segmentControll selectedSegmentIndex] == eCompleted) mapDirectionsPage.isPending = false;
        else mapDirectionsPage.isPending = true;
        
    mapDirectionsPage.isRemovalOrder = _isRemovalOrder;
    [self.navigationController pushViewController:mapDirectionsPage animated:YES];
}


-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    
}

-(void)showPaginationPopUp{
    
    [self.view layoutIfNeeded];
    paginationBottomConstraint.constant = 0;
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

-(void)hidePaginationPopUp{
    
    [self.view layoutIfNeeded];
    paginationBottomConstraint.constant = -40;
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}



-(IBAction)goBack:(id)sender{
    
    [[self navigationController]popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
