//
//  TemplateListViewController.h
//  SignSpot
//
//  Created by Purpose Code on 13/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderListCollectionViewCell.h"

@interface OrderListViewController : UIViewController <ProductCellDelegate>

@property (nonatomic,strong)NSString *strCategoryID;
@property (nonatomic,strong)NSString *strCategoryTitle;
@property (nonatomic,assign)BOOL isRemovalOrder;


@end
