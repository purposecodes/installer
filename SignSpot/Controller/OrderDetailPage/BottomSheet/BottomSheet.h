//
//  BottomSheet.h
//  SignInstaller
//
//  Created by Purpose Code on 12/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BottomSheetDelegate <NSObject>

@optional

-(void)showUploadedImages;

/*!
 *This method is invoked when user taps the 'ROUTE' Button.
 */

-(void)showRouteFromSourceToDestination;

/*!
 *This method is invoked when user taps the 'POINTS' Button.
 */

-(void)showPoints;

/*!
 *This method is invoked when user taps the 'FINISH' Button.
 */

-(void)orderFinishedApplied;



@end




@interface BottomSheet : UIView{
    
    IBOutlet UIButton *btnImageDetails;
    IBOutlet UIButton *btnRouteMap;
}


@property (nonatomic,strong) NSDictionary *details;
@property (nonatomic,weak)  id<BottomSheetDelegate>delegate;
@property (nonatomic,assign)BOOL isPending;
@property (nonatomic,assign)BOOL isRemoval;

-(void)setUp;

@end
