//
//  BottomSheet.m
//  SignInstaller
//
//  Created by Purpose Code on 12/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "BottomSheet.h"
#import "Constants.h"
#import "PhotoBrowser.h"

@interface BottomSheet () <PhotoBrowserDelegate>{
    
    float firstX, firstY;
    IBOutlet UILabel *lblInstalltionDate;
    IBOutlet UILabel *lblOrderNo;
    IBOutlet UILabel *lblQuantity;
    IBOutlet UILabel *lblSubTotal;
    IBOutlet UILabel *lblOrderDate;
    IBOutlet UILabel *lblDueDate;
    IBOutlet UILabel *lblFranchise;
    IBOutlet UILabel *lblInstaller;
    
    IBOutlet UILabel *lblBuyerName;
    IBOutlet UILabel *lblAddress;
    IBOutlet UIImageView *imgTemplate;
    
    BOOL isOpen;
    NSArray *arrImages;
    PhotoBrowser *photoBrowser;
}

@end

@implementation BottomSheet

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setUp{
    
     btnImageDetails.hidden = true;
     btnRouteMap.hidden = true;
    btnImageDetails.layer.cornerRadius = 5.f;
    btnImageDetails.layer.borderWidth = 1.f;
    btnImageDetails.layer.borderColor = [UIColor getThemeColor].CGColor;
    
    if (!_isPending) {
        btnImageDetails.hidden = false;
    }else{
        btnRouteMap.hidden = false;
    }
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(moveViewWithGestureRecognizer:)];
   // [self addGestureRecognizer:panGestureRecognizer];
    if (_details) {
        
        if (NULL_TO_NIL([_details objectForKey:@"order_duedate"]))
            lblInstalltionDate.text = [NSString stringWithFormat:@"Est.Installation Date : %@",[_details objectForKey:@"order_duedate"]];
        
        if (NULL_TO_NIL([_details objectForKey:@"installed_date"]))
            lblInstalltionDate.text = [NSString stringWithFormat:@"Installed Date : %@",[_details objectForKey:@"installed_date"]];
        
        if (NULL_TO_NIL([_details objectForKey:@"removal_duedate"]))
            lblInstalltionDate.text = [NSString stringWithFormat:@"Removal Due Date : %@",[_details objectForKey:@"removal_duedate"]];
        
        if (NULL_TO_NIL([_details objectForKey:@"removed_date"]))
            lblInstalltionDate.text = [NSString stringWithFormat:@"Removed Date : %@",[_details objectForKey:@"removed_date"]];
        
        if (NULL_TO_NIL([_details objectForKey:@"order_no"]))
            lblOrderNo.text = [NSString stringWithFormat:@"Order No : %@",[_details objectForKey:@"order_no"]];
        
        if (NULL_TO_NIL([_details objectForKey:@"totalqty"]))
            lblQuantity.text = [NSString stringWithFormat:@"Quantity : %@",[_details objectForKey:@"totalqty"]];
        
        if (NULL_TO_NIL([_details objectForKey:@"totalprice"]))
            lblSubTotal.text = [NSString stringWithFormat:@"Sub Total : $%.02f",[[_details objectForKey:@"totalprice"] floatValue]];
        
        if (NULL_TO_NIL([_details objectForKey:@"removal_fee"]))
            lblSubTotal.text = [NSString stringWithFormat:@"Removal Fee : $%.02f",[[_details objectForKey:@"removal_fee"] floatValue]];
        
        if (NULL_TO_NIL([_details objectForKey:@"order_date"]))
            lblOrderDate.text = [NSString stringWithFormat:@"Order Date : %@",[_details objectForKey:@"order_date"]];
        
        if (NULL_TO_NIL([_details objectForKey:@"removal_reqdate"]))
            lblOrderDate.text = [NSString stringWithFormat:@"Removal Req. Date : %@",[_details objectForKey:@"removal_reqdate"]];
        
        if (NULL_TO_NIL([_details objectForKey:@"removal_duedate"]))
            lblDueDate.text = [NSString stringWithFormat:@"Due Date : %@",[_details objectForKey:@"removal_duedate"]];
        
        if (NULL_TO_NIL([_details objectForKey:@"order_duedate"]))
            lblDueDate.text = [NSString stringWithFormat:@"Due Date : %@",[_details objectForKey:@"order_duedate"]];
        
        if (NULL_TO_NIL([_details objectForKey:@"franchisee"]))
            lblFranchise.text = [NSString stringWithFormat:@"Franchisee : %@",[_details objectForKey:@"franchisee"]];
        
        if (NULL_TO_NIL([_details objectForKey:@"installer"]))
            lblInstaller.text = [NSString stringWithFormat:@"Installer : %@",[_details objectForKey:@"installer"]];
        
        if (NULL_TO_NIL([_details objectForKey:@"buyer"]))
            lblBuyerName.text = [NSString stringWithFormat:@"Name : %@",[_details objectForKey:@"buyer"]];
        
        if (NULL_TO_NIL([_details objectForKey:@"location_address"]))
            lblAddress.text = [_details objectForKey:@"location_address"];

        if ([_details objectForKey:@"sign_image"] && [[_details objectForKey:@"sign_image"] length]){
            [imgTemplate sd_setImageWithURL:[NSURL URLWithString:[_details objectForKey:@"sign_image"]]
                                  placeholderImage:[UIImage imageNamed:@"NoImage.png"]
                                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                         }];
        }
       
        if ( NULL_TO_NIL([_details objectForKey:@"image"]) )
            arrImages = [NSMutableArray arrayWithArray:[_details objectForKey:@"image"]];
        
        [self showGallery];

        
    }
}

-(void)moveViewWithGestureRecognizer:(UIPanGestureRecognizer*)sender{
    
    [self bringSubviewToFront:[(UIPanGestureRecognizer*)sender view]];
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:[sender view]];
    
    if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan) {
        firstX = [[sender view] center].x;
        firstY = [[sender view] center].y;
    }
    translatedPoint = CGPointMake(firstX, firstY+translatedPoint.y);
    [[sender view] setCenter:translatedPoint];
    
    if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded) {
        CGFloat velocityX = (0.2*[(UIPanGestureRecognizer*)sender velocityInView:[sender view]].x);
        CGFloat finalX = translatedPoint.x; //+ (.2*[(UIPanGestureRecognizer*)sender velocityInView:vwBackground].x);
        CGFloat finalY =  translatedPoint.y + (.2*[(UIPanGestureRecognizer*)sender velocityInView:[sender view]].y);
        UIView *superView = [self superview];
        float height = 300;
        float origin = 100;
        if (_isPending) height = 355;
        else  origin = 70;
        
        if (finalY  <= superView.frame.size.height - height/2) {
            finalY = superView.frame.size.height - height/2;
        }
        if (finalY >= superView.frame.size.height - origin) {
            finalY = superView.frame.size.height + origin;
        }
        CGFloat animationDuration = (ABS(velocityX)*.0002)+.2;
        [UIView animateWithDuration:animationDuration
                         animations:^{
                              isOpen = true;
                              [[sender view] setCenter:CGPointMake(finalX, finalY)];
                             if (finalY > [self superview].frame.size.height) {
                                 isOpen = false;
                             }
                         }
                         completion:^(BOOL finished){
                         }];
        
        
    }

    
    
}

-(void)showGallery{
    
    if (arrImages.count) {
        
        UIButton *btnGallery = [UIButton buttonWithType: UIButtonTypeCustom];
        [btnGallery addTarget: self action: @selector(showGalleryImages) forControlEvents: UIControlEventTouchUpInside];
        btnGallery.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:btnGallery];
        [btnGallery setImage:[UIImage imageNamed:@"Gallery_Image.png"] forState:UIControlStateNormal];
       
        
        [btnGallery addConstraint:[NSLayoutConstraint constraintWithItem:btnGallery
                                                               attribute:NSLayoutAttributeWidth
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:nil
                                                               attribute:NSLayoutAttributeWidth
                                                              multiplier:1.0
                                                                constant:60]];
        [btnGallery addConstraint:[NSLayoutConstraint constraintWithItem:btnGallery
                                                               attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:nil
                                                               attribute:NSLayoutAttributeHeight
                                                              multiplier:1.0
                                                                constant:35]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:btnGallery
                                                             attribute:NSLayoutAttributeLeft
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self
                                                             attribute:NSLayoutAttributeLeft
                                                            multiplier:1.0
                                                              constant:10]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:btnGallery
                                                         attribute:NSLayoutAttributeBottom
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeBottom
                                                        multiplier:1.0
                                                          constant:-10]];
        
        UILabel *lblCount = [UILabel new];
        lblCount.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:lblCount];
        lblCount.font = [UIFont fontWithName:CommonFontBold size:14];
        lblCount.textColor = [UIColor whiteColor];
        lblCount.text = [NSString stringWithFormat:@"%lu",(unsigned long)arrImages.count];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:lblCount
                                                             attribute:NSLayoutAttributeCenterY
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:btnGallery
                                                             attribute:NSLayoutAttributeCenterY
                                                            multiplier:1.0
                                                              constant:2]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:lblCount
                                                             attribute:NSLayoutAttributeLeft
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:btnGallery
                                                             attribute:NSLayoutAttributeLeft
                                                            multiplier:1.0
                                                              constant:40]];
    }
}


-(IBAction)showExapndedSignPostImage{
    
    if ([_details objectForKey:@"sign_image"] && [[_details objectForKey:@"sign_image"] length]){
        NSArray *arrsignImage = [NSArray arrayWithObject:[NSURL URLWithString:[_details objectForKey:@"sign_image"]] ];
        [self presentGalleryWithImages:arrsignImage andIndex:0];
    }
  
}

-(IBAction)showGalleryImages{
    
    [self presentGalleryWithImages:arrImages andIndex:0];
}



- (void)presentGalleryWithImages:(NSArray*)images andIndex:(NSInteger)index
{
    
    if (!photoBrowser) {
        photoBrowser = [[[NSBundle mainBundle] loadNibNamed:@"PhotoBrowser" owner:self options:nil] objectAtIndex:0];
        photoBrowser.delegate = self;
    }
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *vwPopUP = photoBrowser;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser setUpWithImages:images andIndex:index];
    }];
    
}

-(void)closePhotoBrowserView{
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        photoBrowser.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser removeFromSuperview];
        photoBrowser = nil;
    }];
}


-(IBAction)closePopUp:(id)sender{
    CGRect rect;
    float height = 300;
    if (_isPending) height = 355;
    float padding = 70;
    
    float bottomPadding = 0;
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        bottomPadding = window.safeAreaInsets.bottom;
        if (bottomPadding > 0) {
            padding = 45;
        }
    }
     float yPos = bottomPadding + padding;
    
    if (isOpen) {
        rect = CGRectMake(0, [self superview].frame.size.height - yPos, [self superview].frame.size.width, height);
    }else{
         rect = CGRectMake(0, [self superview].frame.size.height - height, [self superview].frame.size.width, height);
    }
    isOpen = !isOpen;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDelegate:self];
    [self setFrame:rect];
    [UIView commitAnimations];
    
}

-(IBAction)showRoute:(UIButton*)sender{
    
    if (sender.tag == 0) {
        sender.tag = 1;
        [sender setImage:[UIImage imageNamed:@"Template.png"] forState:UIControlStateNormal];
        if ([self.delegate respondsToSelector:@selector(showRouteFromSourceToDestination)])
            [self.delegate performSelector:@selector(showRouteFromSourceToDestination)];
    }
    else{
        sender.tag = 0;
        [sender setImage:[UIImage imageNamed:@"Direction.png"] forState:UIControlStateNormal];
        if ([self.delegate respondsToSelector:@selector(showPoints)])
            [self.delegate performSelector:@selector(showPoints)];
    }
   
}
-(IBAction)orderCompleteApplied:(UIButton*)sender{
    if ([self.delegate respondsToSelector:@selector(orderFinishedApplied)])
        [self.delegate performSelector:@selector(orderFinishedApplied)];
    
}

-(IBAction)showUploadedImages:(UIButton*)sender{
    if ([self.delegate respondsToSelector:@selector(showUploadedImages)])
        [self.delegate performSelector:@selector(showUploadedImages)];
    
}



@end
