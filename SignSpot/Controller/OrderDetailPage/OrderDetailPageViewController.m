//
//  NavigationViewController.m
//  SignSpot
//
//  Created by Purpose Code on 01/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "OrderDetailPageViewController.h"
#import "LRouteController.h"
#import "FinishOrderViewController.h"
#import "Constants.h"
#import "BottomSheet.h"
#import "PhotoBrowser.h"

@import GoogleMaps;

@interface OrderDetailPageViewController ()<CLLocationManagerDelegate,GMSMapViewDelegate,BottomSheetDelegate,PhotoBrowserDelegate>{
    
    IBOutlet GMSMapView *_mapView;
    IBOutlet UIButton *btnRefresh;
    IBOutlet UISegmentedControl *segmentControll;
    IBOutlet UILabel *lblTitle;
    
    CLLocationManager *locationManager;
    NSMutableArray *_coordinates;
    LRouteController *_routeController;
    GMSPolyline *_polyline;
    GMSMarker *_markerStart;
    GMSMarker *_markerFinish;
    float zoomLevel;
    PhotoBrowser *photoBrowser;
}

@end

@implementation OrderDetailPageViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setUp];
    [self setUpBottomSheet];
    [self markSignSpot];
    
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)setUp{
    
    zoomLevel = kGMSMaxZoomLevel - 2;
    segmentControll.backgroundColor = [UIColor whiteColor];
    segmentControll.layer.cornerRadius = 5.f;
    _coordinates = [NSMutableArray new];
    _routeController = [LRouteController new];
    
    _mapView.myLocationEnabled = YES;
    _mapView.delegate = self;
    _mapView.settings.rotateGestures = NO;
    _mapView.settings.tiltGestures = NO;
    
    _markerStart = [GMSMarker new];
    _markerStart.title = @"You";
    _mapView.mapType = kGMSTypeSatellite;
    
    _markerFinish = [GMSMarker new];
    //_markerFinish.icon = [UIImage imageNamed:@"MapPointer.png"];
    [btnRefresh setEnabled:false];
    lblTitle.text = @"ORDER DETAILS";
    if (_isRemovalOrder) lblTitle.text = @"REMOVAL ORDER DETAILS";
   
        
    
    
}

#pragma mark - Showing Routes and Positions from BOTTOM SHEET Delegates


-(void)setUpBottomSheet{
    
    BottomSheet* bottomSheet = [[[NSBundle mainBundle] loadNibNamed:@"BottomSheet" owner:self options:nil] objectAtIndex:0];
    if (!_isPending) {
        
    }
    if (bottomSheet) {
        
        float height = 300;
        if (_isPending) {
            height = 355;
        }
        float padding = 70;
        CGFloat bottomPadding = 0;
        if (@available(iOS 11.0, *)) {
            UIWindow *window = UIApplication.sharedApplication.keyWindow;
            bottomPadding = window.safeAreaInsets.bottom;
            if (bottomPadding > 0) {
                padding = 45;
            }
        }
        float yPos = bottomPadding + padding;
        
        bottomSheet.frame = CGRectMake(0, self.view.frame.size.height - yPos, self.view.frame.size.width, height);
        [self.view addSubview:bottomSheet];
        bottomSheet.backgroundColor = [UIColor whiteColor];
        bottomSheet.details = _dictLocationInfo;
        bottomSheet.delegate = self;
        bottomSheet.isPending = _isPending;
        [bottomSheet setUp];
    }
    

}

-(void)showRouteFromSourceToDestination{
    
    [btnRefresh setEnabled:true];
    [self getCurrentUserLocation];
}

-(void)showPoints{
    
     [btnRefresh setEnabled:false];
      [self markSignSpot];
}

-(IBAction)refreshYourLocation:(id)sender{
    
    [self getCurrentUserLocation];
   
}

-(IBAction)changeMapView{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([segmentControll selectedSegmentIndex] == 0) _mapView.mapType =  kGMSTypeSatellite;
        else _mapView.mapType =  kGMSTypeNormal;
        [_mapView reloadInputViews];
    });
    
    
    
    
}

-(void)showUploadedImages{
    
    if ([_dictLocationInfo objectForKey:@"installedimage"]) {
        
        [self presentGalleryWithImages:[_dictLocationInfo objectForKey:@"installedimage"] andIndex:0];
    }
    // Uploded images when finishing order
    
}


-(void)orderFinishedApplied{
    
    FinishOrderViewController *finishOrder =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:MenuDetailsStoryBoard Identifier:StoryBoardIdentifierForOrderFinish];
    finishOrder.templateInfo = _dictLocationInfo;
    finishOrder.isRemovalOrder = _isRemovalOrder;
    [self.navigationController pushViewController:finishOrder animated:YES];
    
}
#pragma mark - Showing All Points On Map

-(void)markSignSpot{
    
    if (_dictLocationInfo) {
        
        [_mapView clear];
        if (NULL_TO_NIL([[_dictLocationInfo objectForKey:@"map"] objectForKey:@"map_path"])) {
            
            NSString *mapPath = [[_dictLocationInfo objectForKey:@"map"] objectForKey:@"map_path"];
            [self drawPolyLineFromSavedPath:mapPath];
        }
        
        if (NULL_TO_NIL([[_dictLocationInfo objectForKey:@"map"] objectForKey:@"location"])) {
            
            float latitude = 0.00;
            float longitude = 0.00;
            float rotation = 0.00;
            
            NSArray *arrLoc = [[_dictLocationInfo objectForKey:@"map"] objectForKey:@"location"];
            NSInteger printType = [[[_dictLocationInfo objectForKey:@"map"] objectForKey:@"printtype"] integerValue];
            NSString *pointerImage = @"Single";
            if (printType == 2) {
                pointerImage = @"Double";
            }
            if (printType == 3) {
                pointerImage = @"Wedge";
            }
            if (arrLoc.count) {
                NSMutableArray *arrCordinates = [NSMutableArray new];
                for (NSDictionary *dict in arrLoc) {
                    
                    if (dict && [dict objectForKey:@"location_latitude"])
                        latitude = [[dict objectForKey:@"location_latitude"]floatValue];
                    if (dict && [dict objectForKey:@"location_longitude"])
                        longitude = [[dict objectForKey:@"location_longitude"]floatValue];
                    if (dict && [dict objectForKey:@"rotation"])
                        rotation = [[dict objectForKey:@"rotation"]floatValue];
                    
                    GMSMarker *marker = [[GMSMarker alloc] init];
                    marker.appearAnimation = kGMSMarkerAnimationPop;
                    marker.position =  CLLocationCoordinate2DMake(latitude,longitude);
                    marker.map = _mapView;
                    marker.tappable = YES;
                    marker.draggable = NO;
                    marker.rotation = rotation;
                    _markerFinish.rotation = rotation;
                    marker.icon = [UIImage imageNamed:pointerImage];
                    _markerFinish.icon = [UIImage imageNamed:pointerImage];
                    _mapView.accessibilityElementsHidden = NO;
                    marker.zIndex = 1;
                    [arrCordinates addObject:[[CLLocation alloc] initWithLatitude:latitude longitude:longitude]];
                    if (NULL_TO_NIL([_dictLocationInfo objectForKey:@"location_address"])) {
                        marker.snippet = [_dictLocationInfo objectForKey:@"location_address"];
                    }
                    
                }
                
                [self focusMapToShowAllMarkers:arrCordinates];
               // GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                                    //    longitude:longitude
                                                                         //    zoom:zoomLevel];
                //_mapView.camera = camera;
                
            }
        }
        
    }
}

-(void)drawPolyLineFromSavedPath:(NSString*)polygonPath{
    
    if (polygonPath.length <= 0) return;
    
    GMSPath *oldpath = [GMSPath pathFromEncodedPath:polygonPath];
    GMSMutablePath *path = [GMSMutablePath path];
    
    for (int i = 0; i < oldpath.count; i++) {
        CLLocationCoordinate2D loc = [oldpath coordinateAtIndex:i];
        [path addCoordinate:loc];
        
    }
    GMSPolyline *_drawnPolyline  = [GMSPolyline polylineWithPath:path];
    _drawnPolyline.strokeColor = [UIColor colorWithRed:255/255.f green:0/255.f blue:18/255.f alpha:1];
    _drawnPolyline.strokeWidth = 4;
    _drawnPolyline.tappable = NO;
    _drawnPolyline.map = _mapView;
    _drawnPolyline.zIndex = 0;
    [_drawnPolyline setTappable:NO];
    [self joinFirstAndLastPoint:polygonPath];
    
    
}

-(void)joinFirstAndLastPoint:(NSString*)polygonPath{
    
    if (polygonPath.length <= 0) return;
    
    GMSPath *oldpath = [GMSPath pathFromEncodedPath:polygonPath];
    GMSMutablePath *path = [GMSMutablePath path];
    
    CLLocationCoordinate2D loc = [oldpath coordinateAtIndex:0];
    [path addCoordinate:loc];
    loc = [oldpath coordinateAtIndex:oldpath.count-1];
    [path addCoordinate:loc];
    
    GMSPolyline *_drawnPolyline  = [GMSPolyline polylineWithPath:path];
    _drawnPolyline.strokeColor = [UIColor colorWithRed:255/255.f green:0/255.f blue:18/255.f alpha:1];
    _drawnPolyline.strokeWidth = 4;
    _drawnPolyline.tappable = NO;
    _drawnPolyline.map = _mapView;
    _drawnPolyline.zIndex = 0;
    [_drawnPolyline setTappable:NO];
}  

-(void)plotAllPointsWith:(NSArray*)points{
    
    for (int i = 0; i < points.count - 1; i ++) {
        
        float latitdue = 0 ;
        float longitude = 0;
        
        NSDictionary *loc = points[i];
        if (loc && [loc objectForKey:@"location_latitude"])
            latitdue = [[loc objectForKey:@"location_latitude"]floatValue];
        if (loc && [loc objectForKey:@"location_longitude"])
            longitude = [[loc objectForKey:@"location_longitude"]floatValue];
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.snippet = _markerFinish.snippet;
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.position =  CLLocationCoordinate2DMake(latitdue, longitude);
        marker.map = _mapView;
       // marker.icon = [UIImage imageNamed:@"MapPointer.png"];
        marker.draggable = NO;
        _mapView.accessibilityElementsHidden = NO;
        marker.zIndex = 1;
        
        
        
    }
    
    
}

#pragma mark - Get user current location


-(void)getCurrentUserLocation{
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if ([locations lastObject]) {
        
        [_coordinates removeAllObjects];
        _markerStart.position = CLLocationCoordinate2DMake(locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude);
         [_coordinates addObject:[[CLLocation alloc] initWithLatitude:locationManager.location.coordinate.latitude longitude:locationManager.location.coordinate.longitude]];
        [locationManager stopUpdatingLocation];
        locationManager = nil;
        [self showDrivingDirections];
        
    }
    
}

#pragma mark - Showing Driving Dierctions


-(void)showDrivingDirections{
    if (_dictLocationInfo) {
        
        float latitdue  = 0;
        float longitude = 0;
        
        if (NULL_TO_NIL([[_dictLocationInfo objectForKey:@"map"] objectForKey:@"location"])) {
            NSArray *arrLoc = [[_dictLocationInfo objectForKey:@"map"] objectForKey:@"location"];
            if (arrLoc.count) {
                NSDictionary *loc = [arrLoc lastObject];
                if (loc && [loc objectForKey:@"location_latitude"])
                    latitdue = [[loc objectForKey:@"location_latitude"]floatValue];
                if (loc && [loc objectForKey:@"location_longitude"])
                    longitude = [[loc objectForKey:@"location_longitude"]floatValue];
                if (arrLoc.count > 1) [self plotAllPointsWith:arrLoc];
                    
            }
        }
        if (NULL_TO_NIL([_dictLocationInfo objectForKey:@"location_address"]))
            _markerFinish.snippet = [_dictLocationInfo objectForKey:@"location_address"];
        _markerFinish.position = CLLocationCoordinate2DMake(latitdue,longitude);
        [self showPathWithLoc:_markerFinish.position];
    }
    
   
}

- (void)showPathWithLoc:(CLLocationCoordinate2D)coordinate
{
    _polyline.map = nil;
    _markerStart.map = nil;
    _markerFinish.map = nil;
    
    [_coordinates addObject:[[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude]];
    
    if ([_coordinates count] > 1)
    {
        [self hideLoadingScreen];
        [self showLoadingScreenWithTitle:@"Loading.."];
        [_routeController getPolylineWithLocations:_coordinates travelMode:TravelModeDriving andCompletitionBlock:^(GMSPolyline *polyline,NSMutableDictionary *distance, NSError *error) {
            if (error)
            {
                NSLog(@"%@", error);
                  [[[UIAlertView alloc] initWithTitle:@"Dierctions" message:@"No Dierctions found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            }
            else if (!polyline)
            {
                [_coordinates removeAllObjects];
                  [[[UIAlertView alloc] initWithTitle:@"Dierctions" message:@"No Dierctions found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            }
            else
            {
                _polyline.map = nil;
                _markerStart.map = nil;
                _markerFinish.map = nil;
                _polyline = nil;
                
                _markerStart.position = [[_coordinates objectAtIndex:0] coordinate];
                _markerStart.map = _mapView;
                
                _markerFinish.position = [[_coordinates lastObject] coordinate];
                _markerFinish.map = _mapView;
                
                _polyline = polyline;
                _polyline.strokeWidth = 3;
                _polyline.strokeColor = [UIColor colorWithRed:255/255.f green:0/255.f blue:18/255.f alpha:1];;
                _polyline.map = _mapView;
            }
            
            [self focusMapToShowAllMarkers:_coordinates];
            [self hideLoadingScreen];
        }];
    }
}

#pragma mark - Generic Methods

#pragma mark - PhotoBrowser

- (void)presentGalleryWithImages:(NSArray*)images andIndex:(NSInteger)index
{
    
    if (!photoBrowser) {
        photoBrowser = [[[NSBundle mainBundle] loadNibNamed:@"PhotoBrowser" owner:self options:nil] objectAtIndex:0];
        photoBrowser.delegate = self;
    }
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *vwPopUP = photoBrowser;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser setUpWithImages:images andIndex:index];
    }];
    
}

- (void)presentGalleryWithImagesnew:(NSArray*)images andIndex:(NSInteger)index
{
    
    if (!photoBrowser) {
        photoBrowser = [[[NSBundle mainBundle] loadNibNamed:@"PhotoBrowser" owner:self options:nil] objectAtIndex:0];
        photoBrowser.delegate = self;
    }
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *vwPopUP = photoBrowser;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser setUpWithImages:images andIndex:index];
    }];
    
}

-(void)closePhotoBrowserView{
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        photoBrowser.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser removeFromSuperview];
        photoBrowser = nil;
    }];
}



-(void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition*)position {
    zoomLevel = mapView.camera.zoom;
    // handle you zoom related logic
}


- (void)focusMapToShowAllMarkers:(NSArray*)_markers;
{
    
    if (NULL_TO_NIL([[_dictLocationInfo objectForKey:@"map"] objectForKey:@"map_path"])) {
        NSString *path = [[_dictLocationInfo objectForKey:@"map"] objectForKey:@"map_path"];
        if (path.length) {
            GMSPath *path = [GMSPath pathFromEncodedPath:[[_dictLocationInfo objectForKey:@"map"] objectForKey:@"map_path"]];
            GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithPath:path];;
            [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:100.f]];
        }else{
            
            [self centerMarkerWhenNoPathAvailable:_markers];
        }
        
    }else{
         [self centerMarkerWhenNoPathAvailable:_markers];
    }
    
   
   
}

-(void)centerMarkerWhenNoPathAvailable:(NSArray*)_markers{
    
     if (_markers.count <= 0 )  {
     return;
     }
     CLLocation *location = _markers[0];
     
     float latitude;
     float longitude;
     
     latitude = location.coordinate.latitude ;
     longitude = location.coordinate.longitude;
     
     
     CLLocationCoordinate2D myLocation = CLLocationCoordinate2DMake(latitude, longitude);
     GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];;
     
     
     for ( CLLocation *location  in _markers){
     
     float latitude;
     float longitude;
     latitude = location.coordinate.latitude ;
     longitude = location.coordinate.longitude;
     CLLocationCoordinate2D coordinates = CLLocationCoordinate2DMake(latitude, longitude);
     bounds = [bounds includingCoordinate:coordinates];
     }
     
     [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:100.0f]];
}

-(void)showLoadingScreenWithTitle:(NSString*)title{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = title;
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}
-(IBAction)goBack:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
