//
//  NavigationViewController.h
//  SignSpot
//
//  Created by Purpose Code on 01/07/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailPageViewController : UIViewController

@property (nonatomic,strong) NSDictionary *dictLocationInfo;
@property (nonatomic,assign)BOOL isRemovalOrder;
@property (nonatomic,assign)BOOL isPending;

@end
