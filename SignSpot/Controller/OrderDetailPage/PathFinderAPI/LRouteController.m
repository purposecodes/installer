#define kDirectionsURL @"https://maps.googleapis.com/maps/api/directions/json?"


#import "LRouteController.h"
#import "Constants.h"

@implementation LRouteController


- (void)getPolylineWithLocations:(NSArray *)locations andCompletitionBlock:(void (^)(GMSPolyline *polyline,NSMutableDictionary *distance, NSError *error))completitionBlock
{
    [self getPolylineWithLocations:locations travelMode:TravelModeDriving andCompletitionBlock:completitionBlock];
}


- (void)getPolylineWithLocations:(NSArray *)locations travelMode:(TravelMode)travelMode andCompletitionBlock:(void (^)(GMSPolyline *polyline,NSMutableDictionary *distance, NSError *error))completitionBlock
{
    NSUInteger locationsCount = [locations count];

    if (locationsCount < 2) return;
    
    NSMutableArray *locationStrings = [NSMutableArray new];

    for (id loc in locations)
    {
        if ([loc isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *location = (NSDictionary*)loc;
            CLLocationDegrees latitude = [location[@"latitude"] doubleValue];
            CLLocationDegrees longitude = [location[@"longitude"] doubleValue];
            [locationStrings addObject:[[NSString alloc] initWithFormat:@"%f,%f",latitude,longitude]];
            
        }else{
            
            CLLocation *location = (CLLocation*)loc;
            CLLocationDegrees latitude = location.coordinate.latitude;
            CLLocationDegrees longitude = location.coordinate.longitude;
            [locationStrings addObject:[[NSString alloc] initWithFormat:@"%f,%f",latitude,longitude]];

        }
        
    }
    
    NSString *sensor = @"false";
    NSString *origin = [locationStrings objectAtIndex:0];
    NSString *destination = [locationStrings lastObject];
    NSMutableString *url = [NSMutableString stringWithFormat:@"%@origin=%@&destination=%@&sensor=%@&key=%@", kDirectionsURL, origin, destination, sensor,GoogleMapAPIKey];
    
    if (locationsCount > 2)
    {
        [url appendString:@"&waypoints=optimize:true"];
        for (int i = 1; i < [locationStrings count] - 1; i++)
        {
            [url appendFormat:@"|%@", [locationStrings objectAtIndex:i]];
        }
    }
    
    switch (travelMode)
    {
        case TravelModeWalking:
            [url appendString:@"&mode=walking"];
            break;
        case TravelModeBicycling:
            [url appendString:@"&mode=bicycling"];
            break;
        case TravelModeTransit:
            [url appendString:@"&mode=transit"];
            break;
        default:
            [url appendString:@"&mode=driving"];
            break;
    }
    
    url = [NSMutableString stringWithString:[url stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
    [APIMapper loaaDrivingDirectionsWithURL:url Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError *error;
        NSDictionary *json = responseObject;
        
        if (!error)
        {
            NSArray *routesArray = [json objectForKey:@"routes"];
            
            if ([routesArray count] > 0)
            {
                NSDictionary *routeDict = [routesArray objectAtIndex:0];
                NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                GMSPath *path = [GMSPath pathFromEncodedPath:points];
                GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
                NSMutableDictionary *dictDistance;
//                if ([[routeDict objectForKey:@"legs"] objectForKey:@"distance"]) {
//                    NSString *distance = [[routeDict objectForKey:@"legs"] objectForKey:@"distance"];
//                    [dictDistance setObject:distance forKey:@"Distance"];
//                }
//                if ([[routeDict objectForKey:@"legs"] objectForKey:@"duration"]) {
//                    NSString *duration = [[routeDict objectForKey:@"legs"] objectForKey:@"duration"];
//                     [dictDistance setObject:duration forKey:@"Duration"];
//                }
                completitionBlock(polyline,dictDistance, nil);
            }
            else
            {
#if DEBUG
                if (locationsCount > 10)
                    NSLog(@"If you're using Google API's free service you will not get the route. Free service supports up to 8 waypoints + origin + destination.");
#endif
                completitionBlock(nil,nil, nil);
            }
        }
        else
        {
            completitionBlock(nil,nil, error);
        }

        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
         completitionBlock(nil,nil, error);
    }];
    }


- (void)abortRequest
{
}


- (void)dealloc
{
    [self abortRequest];
}


@end
