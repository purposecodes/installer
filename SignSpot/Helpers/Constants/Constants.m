

#import "Constants.h"


NSString * const StoryboardForSlider = @"StoryBoardForSlider";
NSString * const StoryboardForLogin = @"Login&RegisterStoryBoard";
NSString * const MenuDetailsStoryBoard = @"MenuDetails";


NSString * const CommonFont = @"HelveticaNeueLTStd-Lt";
NSString * const CommonFontBold = @"HelveticaNeueLTStd-Bd";
NSString * const BaseURLString = @"http://signpostusa.com/shop/installer/api.php?action=";
NSString * const GoogleAutoSearchAPIForPlaceCoordinate = @"https://maps.googleapis.com/maps/api/place/details/json?placeid=%@&key=%@";

//NSString * const GoogleMapAPIKey = @"AIzaSyDLo-Q_ozqGPPl3UZKbyTK_FNfe-GFj7jg";
NSString * const GoogleMapAPIKey = @"AIzaSyCRQ5WW2F1nT-5XDV3hVv4Ii_zv5JVUSXc";



NSString * const StoryBoardIdentifierForLoginPage = @"LoginPage";
NSString * const StoryBoardIdentifierForMenuPage = @"MenuPage";
NSString * const StoryBoardIdentifierForHomePage = @"HomePage";
NSString * const StoryBoardIdentifierForRegistrationPage = @"RegistrationPage";
NSString * const StoryBoardIdentifierForOrderListingPage = @"OrderListingPage";
NSString * const StoryBoardIdentifierForMapPage = @"MapDirectionsPage";
NSString * const StoryBoardIdentifierForQuickView = @"QuickView";
NSString * const StoryBoardIdentifierForOrderFinish = @"OrderFinish";
NSString * const StoryBoardIdentifierForImagePicker = @"ImagePicker";

