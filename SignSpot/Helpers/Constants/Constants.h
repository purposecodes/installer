//
//  Utility.h
//  SignSpot
//
//  Created by Purpose Code on 09/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <Foundation/Foundation.h>

#define NULL_TO_NIL(obj) ({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj; })

extern NSString * const StoryboardForSlider;
extern NSString * const StoryboardForLogin;
extern NSString * const MenuDetailsStoryBoard;

extern NSString * const CommonFont;
extern NSString * const CommonFontBold;
extern NSString * const BaseURLString;

extern NSString * const GoogleMapAPIKey;

extern NSString * const StoryBoardIdentifierForLoginPage;
extern NSString * const StoryBoardIdentifierForRegistrationPage;

extern NSString * const StoryBoardIdentifierForMenuPage ;
extern NSString * const StoryBoardIdentifierForHomePage ;
extern NSString * const StoryBoardIdentifierForOrderListingPage;
extern NSString * const StoryBoardIdentifierForMapPage;
extern NSString * const StoryBoardIdentifierForQuickView;
extern NSString * const StoryBoardIdentifierForOrderFinish;
extern NSString * const StoryBoardIdentifierForImagePicker;


