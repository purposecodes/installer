//
//  Utility.m
//  SignSpot
//
//  Created by Purpose Code on 24/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#define kTagForNodataScreen    1111

#import "Utility.h"
#import "Constants.h"
@import GoogleMaps;

@implementation Utility

+ (void)saveUserObject:(User *)object key:(NSString *)key {
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:encodedObject forKey:key];
    [defaults synchronize];
    
}

+(void)setUpGoogleMapConfiguration{
    
    [GMSServices provideAPIKey:GoogleMapAPIKey];
}

+(void)showNoDataScreenOnView:(UIView*)view withTitle:(NSString*)title{
    
    UIView *noDataScreen =[UIView new];
    noDataScreen.tag = kTagForNodataScreen;
    noDataScreen.backgroundColor = [UIColor whiteColor];
    noDataScreen.translatesAutoresizingMaskIntoConstraints = NO;
    [view addSubview:noDataScreen];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-65-[noDataScreen]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(noDataScreen)]];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[noDataScreen]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(noDataScreen)]];
    
    UILabel *lblTitle = [UILabel new];
    lblTitle.text = title;
    lblTitle.translatesAutoresizingMaskIntoConstraints = NO;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.font = [UIFont fontWithName:CommonFont size:17];
    lblTitle.textColor = [UIColor lightGrayColor];
    [noDataScreen addSubview:lblTitle];
    [noDataScreen addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[lblTitle]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle)]];
    [noDataScreen addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[lblTitle]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle)]];
   
}

+(void)removeNoDataScreen:(UIView*)_view{
    
    if ([_view viewWithTag:kTagForNodataScreen]) {
        
        [[_view viewWithTag:kTagForNodataScreen] removeFromSuperview];
    }
}
+(UITableViewCell *)getNoDataCustomCellWith:(UITableView*)aTableView withTitle:(NSString*)title{
    
    static NSString *MyIdentifier = @"MyIdentifier";
    UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    cell.textLabel.text = title;
    cell.textLabel.font = [UIFont fontWithName:CommonFont size:17];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.textColor = [UIColor grayColor];
    cell.backgroundColor = [UIColor getBackgroundOffWhiteColor];
    return cell;
}

+(void)removeAViewControllerFromNavStackWithType:(Class)vc from:(NSArray*)array{
    
    for(UIViewController *tempVC in array)
    {
        if([tempVC isKindOfClass:vc])
        {
            [tempVC removeFromParentViewController];
        }
    }

    
}

+(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


@end
