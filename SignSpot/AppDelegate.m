//
//  AppDelegate.m
//  SignSpot
//
//  Created by Purpose Code on 09/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"
#import "MenuViewController.h"
#import "Constants.h"
#import <UserNotifications/UserNotifications.h>
#import "JCNotificationCenter.h"
#import "JCNotificationBannerPresenterIOSStyle.h"
#import "OrderListViewController.h"

@import Firebase;

@interface AppDelegate () <FIRMessagingDelegate,UNUserNotificationCenterDelegate>{
    NSString *token;
    
    
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Override point for customization after application launch.
    [Utility setUpGoogleMapConfiguration];
    [FIRApp configure];
    [self checkUserStatus];
    [self enablePushNotification];
    [self resetBadgeCount];
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    
    for (NSString *param in [[url query] componentsSeparatedByString:@"&"]) {
        NSArray *elts = [param componentsSeparatedByString:@"="];
        if([elts count] < 2) continue;
        //NSLog(@"%@",elts);
        NSString *type = elts[1];
        if ([type isEqualToString:@"install_order"]) {
            [self openOrderListPage];
        }else {
            [self openRemovalOrderListPage];
        }
    }
    
    return true;

}

#pragma mark - Push notifications Actions

-(void)enablePushNotification{
    
    [FIRMessaging messaging].delegate = self;
    
    if ([UNUserNotificationCenter class] != nil) {
        // iOS 10 or later
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter]
         requestAuthorizationWithOptions:authOptions
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                  [self sendTokenToBackend];
             });

             // ...
         }];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    
    
}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    if (fcmToken.length) {
        token = fcmToken;
       
    }
   
    // TODO: If necessary send token to application server.
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    [self resetBadgeCount];
    if(application.applicationState == UIApplicationStateInactive) {
        [self handleNotificationWhenBackGroundWith:userInfo];
        completionHandler(UIBackgroundFetchResultNewData);
        
    } else if (application.applicationState == UIApplicationStateBackground) {
        
        completionHandler(UIBackgroundFetchResultNewData);
        
    } else {
        [self handleNotificationWhenForeGroundWith:userInfo];
        completionHandler(UIBackgroundFetchResultNewData);
        
    }
}

-(void)resetBadgeCount{
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}
-(void)sendTokenToBackend{
    
    if (token.length && [User sharedManager].userId.length) {
        [APIMapper setPushNotificationTokenWithToken:token success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            
        }];
    }
   
}



-(void)handleNotificationWhenBackGroundWith:(NSDictionary*)userInfo{
    
    if ([[userInfo objectForKey:@"notification_type"] isEqualToString:@"install_order"]) {
        [self openOrderListPage];
    }if ([[userInfo objectForKey:@"notification_type"] isEqualToString:@"removal_order"]) {
        [self openRemovalOrderListPage];
    }
    
}
-(void)handleNotificationWhenForeGroundWith:(NSDictionary*)userInfo{
    
    NSString *message = @"SignPost";
    NSString *Title = @"SignPostInstaller";
    if ([[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]objectForKey:@"title"])
        Title = [[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"title"];
    if ([[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"body"])
        message = [[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] objectForKey:@"body"];
    
    [JCNotificationCenter sharedCenter].presenter = [JCNotificationBannerPresenterIOSStyle new];
    [JCNotificationCenter enqueueNotificationWithTitle:Title message:message tapHandler:^{
        
        if ([[userInfo objectForKey:@"notification_type"] isEqualToString:@"install_order"]) {
            [self openOrderListPage];
        }if ([[userInfo objectForKey:@"notification_type"] isEqualToString:@"removal_order"]) {
            [self openRemovalOrderListPage];
        }
        
    }];
}

-(void)openOrderListPage{
    
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: _navHome.viewControllers];
    for (UIViewController *vc in navigationArray) {
        if ([vc isKindOfClass:[OrderListViewController class]]) {
            [navigationArray removeObjectIdenticalTo:vc];
        }
    }
    _navHome.viewControllers = navigationArray;
    HomeViewController *homeVC = [_navHome.viewControllers objectAtIndex:0];
    [homeVC showOrderListings];
   
    
}

-(void)openRemovalOrderListPage{
    
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: _navHome.viewControllers];
    for (UIViewController *vc in navigationArray) {
        if ([vc isKindOfClass:[OrderListViewController class]]) {
            [navigationArray removeObjectIdenticalTo:vc];
        }
    }
    _navHome.viewControllers = navigationArray;
    HomeViewController *homeVC = [_navHome.viewControllers objectAtIndex:0];
    [homeVC showOrdersWhichAreToBeRemoved];
    
    
}


#pragma mark - Login Actions

-(void)checkUserStatus{
    
    if ([User sharedManager]) self.currentUser = (User*)[User sharedManager];
    BOOL userExists = [self loadUserObjectWithKey:@"USER"];
    if (userExists) [self showHomeScreen];
    else            [self showLoginScreen];

}

/*!.........Check Availability of User !...........*/

- (BOOL )loadUserObjectWithKey:(NSString *)key {
    BOOL isUserExists = false;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:key];
    if (encodedObject) {
        User *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
        if (object)isUserExists = true;
        [self createUserObject:object];
        
    }
    return isUserExists;
}

-(void)createUserObject:(User*)user{
    
    // Creating singleton user object with Decoded data from NSUserDefaults
    
    [User sharedManager].isLoggedIn = user.isLoggedIn;
    [User sharedManager].userId = user.userId;
    [User sharedManager].userTypeId = user.userTypeId;
    [User sharedManager].name = user.name;
    [User sharedManager].email = user.email;
    [User sharedManager].regDate = user.regDate;
    [User sharedManager].loggedStatus = user.loggedStatus;
    [User sharedManager].verifiedStatus = user.verifiedStatus;
    [User sharedManager].profileurl = user.profileurl;
    [User sharedManager].cartCount = user.cartCount;
    [User sharedManager].notificationCount = user.notificationCount;
    [User sharedManager].companyID = user.companyID;
    
}

-(void)showLoginScreen{
    
    // Show login page for a  user.
    
    LoginViewController *loginPage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:StoryboardForLogin Identifier:StoryBoardIdentifierForLoginPage];
    loginPage.delegate = self;
    UINavigationController *logginVC = [[UINavigationController alloc] initWithRootViewController:loginPage];
    logginVC.navigationBarHidden = true;
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    self.window.rootViewController = logginVC;
    [self.window makeKeyAndVisible];
    
    
}

// Success call back from login after successful attempt

-(void)goToHomeAfterLogin{
    
    [self showHomeScreen];
}



- (void)showHomeScreen {
    
    // Show home screen once login is successful.
    

    HomeViewController *homeVC =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:StoryboardForSlider Identifier:StoryBoardIdentifierForHomePage];
    UINavigationController *navHome = [[UINavigationController alloc] initWithRootViewController:homeVC];
    _navHome = navHome;
    MenuViewController *menuVC =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:StoryboardForSlider Identifier:StoryBoardIdentifierForMenuPage];
    UINavigationController *navMenu = [[UINavigationController alloc] initWithRootViewController:menuVC];
    navMenu.navigationBarHidden = true;
    SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:navMenu frontViewController:navHome];
    
    [UIView transitionWithView:self.window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{ self.window.rootViewController = revealController; }
                    completion:nil];
    
    
   
    
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.sample.SignSpot" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"SignSpot" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"SignSpot.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}




@end
