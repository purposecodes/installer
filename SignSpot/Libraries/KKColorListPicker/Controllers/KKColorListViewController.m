//
//  KKColorListViewController.m
//  KKColorListPickerExample
//
//  Created by Kirill Kunst on 28.12.13.
//  Copyright (c) 2013 Kirill Kunst. All rights reserved.
//

#import "KKColorListViewController.h"
#import "KKColorsHeaderView.h"
#import "KKColorCell.h"
#import "KKColor.h"
#import "KKColorsLoader.h"
#import "UIColor+CustomColors.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"

@interface KKColorListViewController () <UICollectionViewDelegate, UICollectionViewDataSource, KKColorsHeaderViewDelegate>{
    
    BOOL isForBodrer;
    BOOL isTextColor;
}

@property (nonatomic, strong) UICollectionView *colorsCollection;

@property (nonatomic) KKColorsSchemeType currentScheme;
@property (nonatomic, strong) NSArray *colors;

- (void)actionClose:(id)sender;

@end


@implementation KKColorListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (id)initWithSchemeType:(KKColorsSchemeType)schemeType isForBorder:(BOOL)isBorder isTextColor:(BOOL)textColor
{
    self = [self init];
    if (self) {
        self.currentScheme = schemeType;
        isForBodrer = isBorder;
        isTextColor = textColor;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    //
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)initUI
{
    [self setUpNavigationHeader];
    [self createCollectionView];
    [self.colorsCollection registerNib:[UINib nibWithNibName:@"KKColorCell" bundle:nil] forCellWithReuseIdentifier:@"KKColorCell"];
    [self.colorsCollection registerNib:[UINib nibWithNibName:@"KKColorsHeaderView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"KKColorsHeaderView"];
    self.view.backgroundColor = self.backgroundColor;
    [KKColorsLoader loadColorsForType:self.currentScheme completion:^(NSArray *colors) {
        self.colors = colors;
        [self.colorsCollection reloadData];
    }];
    self.view.backgroundColor = [UIColor whiteColor];
    
}

-(void)setUpNavigationHeader{
    
    UIView *vwHeader = [UIView new];
    vwHeader.backgroundColor = [UIColor getThemeColor];
    vwHeader.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:vwHeader];
    
    
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:vwHeader
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1.0
                                                           constant:65.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:vwHeader
                                                         attribute:NSLayoutAttributeTop
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.view
                                                         attribute:NSLayoutAttributeTop
                                                        multiplier:1.0
                                                          constant:0.0]];

    
     NSArray *verticalConstraints =[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwHeader]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwHeader)];
    [self.view addConstraints:verticalConstraints];
    
    UIButton* btnCancel = [UIButton new];
    btnCancel.backgroundColor = [UIColor clearColor];
    btnCancel.translatesAutoresizingMaskIntoConstraints = NO;
    [btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnCancel.titleLabel.font = [UIFont fontWithName:CommonFont size:14];
    [btnCancel addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    [btnCancel setImage:[UIImage imageNamed:@"BackArrow.png"] forState:UIControlStateNormal];
    [vwHeader addSubview:btnCancel];
    btnCancel.imageEdgeInsets = UIEdgeInsetsMake(0, -50, 0, 0);
    
    [btnCancel addConstraint:[NSLayoutConstraint constraintWithItem:btnCancel
                                                           attribute:NSLayoutAttributeWidth
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeWidth
                                                          multiplier:1.0
                                                            constant:100.0]];
    
    [btnCancel addConstraint:[NSLayoutConstraint constraintWithItem:btnCancel
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1.0
                                                           constant:45.0]];

    
    
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:btnCancel
                                                          attribute:NSLayoutAttributeLeft
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:vwHeader
                                                          attribute:NSLayoutAttributeLeft
                                                         multiplier:1.0
                                                           constant:0.0]];
    
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:btnCancel
                                                         attribute:NSLayoutAttributeTop
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:vwHeader
                                                         attribute:NSLayoutAttributeTop
                                                        multiplier:1.0
                                                          constant:20.0]];
    
    
    UILabel *lblTitle = [UILabel new];
    lblTitle.translatesAutoresizingMaskIntoConstraints = NO;
    [vwHeader addSubview:lblTitle];
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[lblTitle]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle)]];
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-100-[lblTitle]-100-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle)]];
    lblTitle.text = @"Pick A Color";
    lblTitle.font = [UIFont fontWithName:CommonFont size:16];
    lblTitle.textColor = [UIColor blackColor];
}

- (void)createCollectionView
{
    UICollectionViewFlowLayout *aFlowLayout = [[UICollectionViewFlowLayout alloc] init];
    [aFlowLayout setItemSize:CGSizeMake(36, 36)];
    [aFlowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];

    self.colorsCollection = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:aFlowLayout];
    [self.view addSubview:self.colorsCollection];
    NSDictionary* viewDict = @{@"collectionView": self.colorsCollection};
    self.colorsCollection.delegate = self;
    self.colorsCollection.dataSource = self;
    self.colorsCollection.backgroundColor = [UIColor whiteColor];
    self.colorsCollection.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *horizontalConstraints =[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-65-[collectionView]-0-|" options:0 metrics:nil views:viewDict];
    [self.view addConstraints:horizontalConstraints];
     horizontalConstraints =[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[collectionView]-0-|" options:0 metrics:nil views:viewDict];
    [self.view addConstraints:horizontalConstraints];
    
    
}

- (void)actionCancel
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(colorListPickerDidComplete:)]) {
        [self.delegate colorListPickerDidComplete:self];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    
    return self.colors.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    KKColorCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"KKColorCell" forIndexPath:indexPath];
    KKColor *color = self.colors[indexPath.row];
    [cell setColor:color];
    [cell setSelectedColor:self.selectedCellBorderColor selectedBorderWidth:self.selectedCellBorderWidth];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        KKColorsHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"KKColorsHeaderView" forIndexPath:indexPath];
        headerView.titleLabel.text = [self headerTitle];
        headerView.titleLabel.textColor = [self headerTitleTextColor];
        headerView.closeButton.titleLabel.textColor = [self headerTitleTextColor];
        [headerView.closeButton setTitleColor:[self headerTitleTextColor] forState:UIControlStateNormal];
        headerView.delegate = self;
        reusableview = headerView;
    }
    return reusableview;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(36, 36);
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(2, 2, 2, 2);
}



- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    KKColor *color = self.colors[indexPath.row];
    if (self.delegate) {
        [self.delegate colorListController:self didSelectColor:color isForBorder:isForBodrer isTextColor:isTextColor];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Properties
- (NSString *)headerTitle
{
    if (!_headerTitle) {
        _headerTitle = @"Choose color";
    }
    return _headerTitle;
}

- (UIColor *)selectedCellBorderColor
{
    if (!_selectedCellBorderColor) {
        _selectedCellBorderColor = [UIColor whiteColor];
    }
    return _selectedCellBorderColor;
}

- (CGFloat)selectedCellBorderWidth
{
    if (!_selectedCellBorderWidth) {
        _selectedCellBorderWidth = 2.0f;
    }
    return _selectedCellBorderWidth;
}

- (UIColor *)backgroundColor
{
    if (!_backgroundColor) {
        _backgroundColor = [UIColor colorFromHexString:@"1b1b1b"];
    }
    return _backgroundColor;
}

- (UIColor *)navBarTitleColor
{
    if (!_navBarTitleColor) {
        _navBarTitleColor = [UIColor blackColor];
    }
    return _navBarTitleColor;
}

- (UIColor *)headerTitleTextColor
{
    if (!_headerTitleTextColor) {
        _headerTitleTextColor = [UIColor whiteColor];
    }
    return _headerTitleTextColor;
}

- (NSArray *)colors
{
    if (!_colors) {
        _colors = [NSArray array];
    }
    return _colors;
}

- (void)didClickCloseButton:(KKColorsHeaderView *)view
{
    [self actionCancel];
}

@end
