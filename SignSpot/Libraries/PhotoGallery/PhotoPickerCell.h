//
//  PhotoCell.h
//  CustomImagePicker
//
//  Created by Prasanna Nanda on 1/5/15.
//  Copyright (c) 2015 Prasanna Nanda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"

@interface PhotoPickerCell : UICollectionViewCell

@property (nonatomic,weak) IBOutlet UILabel *lblError;
- (void) setImage:(UIImage *)image;
- (UIImageView*) getImageView;
-(void) performSelectionAnimations;
-(void) hideTick;
-(void) showTick;
@end
