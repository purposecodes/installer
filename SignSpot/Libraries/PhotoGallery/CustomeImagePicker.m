//
//  CustomeImagePicker.m
//  CustomImagePicker
//
//  Created by Prasanna Nanda on 1/5/15.
//  Copyright (c) 2015 Prasanna Nanda. All rights reserved.
//

#import "CustomeImagePicker.h"
#import "UIView+RNActivityView.h"
#import <Photos/Photos.h>
#import "AlbumTitleCell.h"
#import "Constants.h"
#import "UIColor+Color.h"

@interface CustomeImagePicker () <UIGestureRecognizerDelegate>{
    
    PHImageRequestOptions *requestOptions;
    NSMutableDictionary *dictAssets;
    NSMutableArray *allTitles;
    IBOutlet UITableView *tableView;
    IBOutlet UILabel *lblAlbumName;
    IBOutlet UIView *vwAlbumOverlay;
    IBOutlet UIView *vwToolTip;
    
    
}
@property(nonatomic, strong) NSArray *assets;

@end

@implementation CustomeImagePicker
@synthesize skipButton,nextButton,hideNextButton,hideSkipButton,highLightThese,maxPhotos,distanceFromButton;

-(BOOL) checkForCamera
{
    return NO;
}
-(void) viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  if(highLightThese == Nil)
    highLightThese = [[NSMutableArray alloc] init];

}
-(void) viewDidDisappear:(BOOL)animated
{
  [super viewDidDisappear:animated];
 }
-(void) viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
}

- (void)viewDidLoad
{
  [super viewDidLoad];
    
    _collectionView.alwaysBounceVertical = YES;
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideAlbumTable)];
    [tapGesture setNumberOfTapsRequired:1];
    tapGesture.delegate = self;
    [vwAlbumOverlay addGestureRecognizer:tapGesture];
    
    
    dictAssets = [NSMutableDictionary new];
    requestOptions = [[PHImageRequestOptions alloc] init];
    requestOptions.deliveryMode =  PHImageRequestOptionsDeliveryModeHighQualityFormat;
    requestOptions.resizeMode = PHImageRequestOptionsResizeModeNone;
    requestOptions.synchronous = NO;
    requestOptions.networkAccessAllowed = YES;
    [skipButton setHidden:YES];
    [nextButton setHidden:YES];
    
    nextButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    nextButton.layer.borderWidth = 1.0f;
    nextButton.layer.cornerRadius = 5.0f;
    
    skipButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    skipButton.layer.borderWidth = 1.0f;
    skipButton.layer.cornerRadius = 5.0f;
    
    
    UIBezierPath *aPath = [UIBezierPath bezierPath];
    [aPath moveToPoint:CGPointMake(0.0, 20.0)];
    [aPath addLineToPoint:CGPointMake(10, 0)];
    [aPath addLineToPoint:CGPointMake(20, 20)];
    CAShapeLayer *border = [CAShapeLayer layer];
    border.path = aPath.CGPath;
    border.fillColor = [UIColor whiteColor].CGColor;
    [vwToolTip.layer addSublayer:border];
    
    
    [self.collectionView registerClass:[PhotoPickerCell class] forCellWithReuseIdentifier:@"PhotoPickerCell"];
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [self.collectionView setCollectionViewLayout:flowLayout];
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        
        if (status == PHAuthorizationStatusAuthorized) {
            
            PHAssetMediaType mediaType = PHAssetMediaTypeImage;
           
            PHFetchOptions *options = [[PHFetchOptions alloc] init];
            options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
            
            PHFetchResult *syncedAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum
                                                                                   subtype:PHAssetCollectionSubtypeAlbumRegular options:nil];
            
            [syncedAlbums enumerateObjectsUsingBlock:^(PHAssetCollection *collection, NSUInteger idx, BOOL *stop) {
                PHFetchResult *collectionResult = [PHAsset fetchAssetsInAssetCollection:collection options:options];
                NSMutableArray *arr = [NSMutableArray new];
                [collectionResult enumerateObjectsUsingBlock:^(PHAsset *asset, NSUInteger idx, BOOL *stop) {
                    
                    if (asset.mediaType == mediaType) [arr addObject:asset];
                    
                }];
                if (arr.count && collection.localizedTitle) [dictAssets setObject:arr forKey:collection.localizedTitle];
                
            }];
            
            
            PHFetchResult *smartAlbumss = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:nil];
            
            [smartAlbumss enumerateObjectsUsingBlock:^(PHAssetCollection *collection, NSUInteger idx, BOOL *stop) {
                
                
                PHFetchResult *collectionResult = [PHAsset fetchAssetsInAssetCollection:collection options:options];
                NSMutableArray *arr = [NSMutableArray new];
                [collectionResult enumerateObjectsUsingBlock:^(PHAsset *asset, NSUInteger idx, BOOL *stop) {
                    
                    if (asset.mediaType == mediaType) [arr addObject:asset];
                    
                }];
                
                if (arr.count && collection.localizedTitle) [dictAssets setObject:arr forKey:collection.localizedTitle];
                
            }];
            
            PHFetchResult *assetsFetchResults = [PHAsset fetchAssetsWithOptions:options];
            NSMutableArray *arr = [NSMutableArray new];
            [assetsFetchResults enumerateObjectsUsingBlock:^(PHAsset *asset, NSUInteger idx, BOOL *stop) {
                
                if (asset.mediaType == mediaType) [arr addObject:asset];
                
            }];
            dispatch_async(dispatch_get_main_queue(), ^{
                NSArray *anArray = [dictAssets allKeys];
                allTitles = [NSMutableArray arrayWithArray:[anArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]] ;
                NSInteger maxCount = 0;
                NSString *albumName;
                for (NSString *str in allTitles) {
                    if ([[dictAssets objectForKey:str] isKindOfClass:[NSArray class]]) {
                        NSArray *phots = [dictAssets objectForKey:str];
                        NSInteger count = phots.count;
                        if (count > maxCount) {
                            maxCount = count;
                            albumName = str;
                        }
                    }
                  
                }
                NSInteger anIndex=[allTitles indexOfObject:albumName];
                if(NSNotFound == anIndex) {
                }else[allTitles exchangeObjectAtIndex:0 withObjectAtIndex:anIndex];
                [tableView reloadData];
            });
            maxPhotos = 10;
            [self loadAllPhotosWithArray:arr];
           
        }else{
            [self displayErrorOnMainQueue:@"Photo Access Disabled" message:@"Please allow Photo Access in System Settings"];
        }
        
        
    }];
    
  
 
  

}
-(void)loadAllPhotosWithArray:(NSMutableArray*)assets{
    
    self.assets = assets;
    dispatch_time_t popTime1 = dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC);
    dispatch_after(popTime1, dispatch_get_main_queue(), ^(void){
        [self.collectionView reloadData];
    });
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)displayErrorOnMainQueue:(NSString*) heading message:(NSString *)message
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:heading
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                            
                         }];
    
   
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}
- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
  return self.assets.count;
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *cellIdentifier = @"PhotoPickerCell";
    
    PhotoPickerCell *cell = (PhotoPickerCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.lblError.hidden = true;
    PHAsset *asset = self.assets[indexPath.row];
    PHImageManager *manager = [PHImageManager defaultManager];
    [manager requestImageForAsset:asset
                       targetSize:CGSizeMake(300, 300)
                      contentMode:PHImageContentModeDefault
                          options:requestOptions
                    resultHandler:^void(UIImage *image, NSDictionary *info) {
                        if (!image) cell.lblError.hidden = false;
                        [cell setImage:image];
                        
                    }];
    
    if([highLightThese containsObject:asset])
    {
        cell.layer.borderColor = [[UIColor orangeColor] CGColor];
        cell.layer.borderWidth = 4.0;
        [cell setAlpha:1.0];
    }
    else
    {
        cell.layer.borderColor = nil;
        cell.layer.borderWidth = 0.0;
        [cell setAlpha:1.0];
    }

  return cell;
}

- (CGSize)collectionView:(UICollectionView *)_collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    float width = (_collectionView.frame.size.width / 3) - 5;
    float height = width;
    return CGSizeMake(width, height);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 5.0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < self.assets.count) {
        PHAsset *asset = self.assets[indexPath.row];
        if (asset) {
            
            PhotoPickerCell *datasetCell = (PhotoPickerCell *)[collectionView cellForItemAtIndexPath:indexPath];
            if ([highLightThese containsObject:asset]) {
                
                [highLightThese removeObject:asset];
                datasetCell.layer.borderColor = nil;
                datasetCell.layer.borderWidth = 0.0;
                [datasetCell setAlpha:1.0];
                [datasetCell setUserInteractionEnabled:YES];
                 _lblMediaCount.text = [NSString stringWithFormat:@"%lu/10",(unsigned long)highLightThese.count];
                
                
            }else{
                
                if([highLightThese count] == maxPhotos)
                {
                    return;
                }
                [highLightThese addObject:asset];
                datasetCell.layer.borderColor = [[UIColor orangeColor] CGColor];
                datasetCell.layer.borderWidth = 4.0;
                [datasetCell setAlpha:1.0];
                [datasetCell setUserInteractionEnabled:YES];
                _lblMediaCount.text = [NSString stringWithFormat:@"%lu/10",(unsigned long)highLightThese.count];
                
            }

        }
        if([highLightThese count]>=1)
            [nextButton setHidden:NO];
        else
            [nextButton setHidden:YES];

    }
    
}

#pragma mark - TableView Delegates


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [allTitles count];
}


- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AlbumTitleCell";
    AlbumTitleCell *cell = (AlbumTitleCell *)[_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (indexPath.row < allTitles.count) {
        
        NSArray *assets = [dictAssets objectForKey:[allTitles objectAtIndex:indexPath.row]];
        cell.lblTitle.text = [NSString stringWithFormat:@"%@ (%lu)",[allTitles objectAtIndex:indexPath.row],(unsigned long)assets.count];
        PHAsset *asset = assets[0];
        PHImageManager *manager = [PHImageManager defaultManager];
        [manager requestImageForAsset:asset
                           targetSize:CGSizeMake(50, 50)
                          contentMode:PHImageContentModeDefault
                              options:requestOptions
                        resultHandler:^void(UIImage *image, NSDictionary *info) {
                            [cell.imgView setImage:image];
                            
                        }];
        
        
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 80;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row < allTitles.count) {
        NSArray *assets = [dictAssets objectForKey:[allTitles objectAtIndex:indexPath.row]];
        lblAlbumName.text = [allTitles objectAtIndex:indexPath.row];
        self.assets = [NSArray arrayWithArray:assets];
        _lblMediaCount.text = @"";
        [highLightThese removeAllObjects];
        [_collectionView reloadData];
        [self hideAlbumTable];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 0.01;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return  0.01;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:tableView])
        return NO;
    return YES;
}

-(void)hideAlbumTable{
    
    [UIView animateWithDuration:0.4 animations:^(void) {
        vwAlbumOverlay.alpha = 0;
    }];
}
-(IBAction)showAlbumTable{
    
    [UIView animateWithDuration:0.4 animations:^(void) {
        vwAlbumOverlay.alpha = 1;
    }];
}


-(IBAction)donePressed:(id)sender
{
  if([highLightThese count] == 0)
  {
    NSLog(@"Please Select One");
  }
  else
  {
    NSMutableArray *allImagesIPicked = [[NSMutableArray alloc] init];
    for(NSString *ip in highLightThese)
    {
      [allImagesIPicked addObject:ip];
    } // end of for loop
    [self dismissViewControllerAnimated:YES completion:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([self.delegate respondsToSelector:@selector(imageSelected:isPhoto:)]) {
                [self.delegate imageSelected:allImagesIPicked isPhoto:YES];
            }
        });
        
     
    }];

  }
}
-(IBAction)skipPressed:(id)sender
{
//  self.selectedImage = Nil;
  [self dismissViewControllerAnimated:YES completion:^{
    if ([self.delegate respondsToSelector:@selector(imageSelected:isPhoto:)]) {
      [self.delegate imageSelected:Nil isPhoto:YES];
    }
  }];
}
-(UIImage *)imageByScalingAndCroppingForSize:(CGSize)targetSize sourceImage:(UIImage*)sourceImage
{
  UIImage *newImage = nil;
  CGSize imageSize = sourceImage.size;
  CGFloat width = imageSize.width;
  CGFloat height = imageSize.height;
  CGFloat targetWidth = targetSize.width;
  CGFloat targetHeight = targetSize.height;
  CGFloat scaleFactor = 0.0;
  CGFloat scaledWidth = targetWidth;
  CGFloat scaledHeight = targetHeight;
  CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
  
  if (CGSizeEqualToSize(imageSize, targetSize) == NO)
  {
    CGFloat widthFactor = targetWidth / width;
    CGFloat heightFactor = targetHeight / height;
    
    if (widthFactor > heightFactor)
    {
      scaleFactor = widthFactor; // scale to fit height
    }
    else
    {
      scaleFactor = heightFactor; // scale to fit width
    }
    
    scaledWidth  = width * scaleFactor;
    scaledHeight = height * scaleFactor;
    
    // center the image
    if (widthFactor > heightFactor)
    {
      thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
    }
    else
    {
      if (widthFactor < heightFactor)
      {
        thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
      }
    }
  }
  
  UIGraphicsBeginImageContext(targetSize); // this will crop
  //UIGraphicsBeginImageContextWithOptions(targetSize, 1.0, 0.0);
  
  CGRect thumbnailRect = CGRectZero;
  thumbnailRect.origin = thumbnailPoint;
  thumbnailRect.size.width  = scaledWidth;
  thumbnailRect.size.height = scaledHeight;
  
  [sourceImage drawInRect:thumbnailRect];
  
  newImage = UIGraphicsGetImageFromCurrentImageContext();
  
  if(newImage == nil)
  {
    NSLog(@"could not scale image");
  }
  
  //pop the context to get back to the default
  UIGraphicsEndImageContext();
  
  return newImage;
}


-(IBAction)cancelPressed:(id)sender
{
//  self.selectedImage = Nil;
  [self dismissViewControllerAnimated:YES completion:^{
    if ([self.delegate respondsToSelector:@selector(imageSelectionCancelled)]) {
      [self.delegate imageSelectionCancelled];
    }
  }];
  
}



/* YCameraView Delegate End */


@end
