//
//  PhotoBrowser.m
//  PurposeColor
//
//  Created by Purpose Code on 08/09/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "PhotoBrowser.h"
#import "PhotoBrowserCell.h"

@interface PhotoBrowser(){
    
    IBOutlet UICollectionView *collectionView;
    IBOutlet UILabel *lblTitle;
    NSArray *arrDataSource;
    NSInteger curentIndex;
}

@end

@implementation PhotoBrowser

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setUpWithImages:(NSArray*)images andIndex:(NSInteger)index{
    
    arrDataSource = images;
    [collectionView registerNib:[UINib nibWithNibName:@"PhotoBrowserCell" bundle:nil] forCellWithReuseIdentifier:@"PhotoBrowserCell"];
    collectionView.backgroundColor = [UIColor clearColor];
    [collectionView reloadData];
    curentIndex = index + 1;
    [collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];

    
}



#pragma mark - UICollectionViewDataSource Methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
     return  1;
}

-(NSInteger)collectionView:(UICollectionView *)_collectionView numberOfItemsInSection:(NSInteger)section
{
    lblTitle.text = [NSString stringWithFormat:@"%d/%lu",curentIndex,(unsigned long)arrDataSource.count];
    return  arrDataSource.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)_collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PhotoBrowserCell *cell = [_collectionView dequeueReusableCellWithReuseIdentifier:@"PhotoBrowserCell" forIndexPath:indexPath];
    if (indexPath.row < arrDataSource.count) {
        [cell.indicator stopAnimating];
        [cell resetCell];
         id object = arrDataSource[indexPath.row];
        if ([object isKindOfClass:[NSDictionary class]]) {
            NSDictionary *details = arrDataSource[indexPath.row];
            if ([details objectForKey:@"image_url"]) {
                NSString *strUrl = [details objectForKey:@"image_url"];
                [cell.indicator startAnimating];
                [cell.imageView sd_setImageWithURL:[NSURL URLWithString:strUrl]
                                  placeholderImage:[UIImage imageNamed:@"NoImage.png"]
                                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                             cell.image = image;
                                             [cell setup];
                                             [cell.indicator stopAnimating];
                                             
                                             
                                         }];
            }
           
        }else if ([object isKindOfClass:[NSURL class]]){
            NSURL *url = (NSURL*)object;
            [cell.indicator startAnimating];
            [cell.imageView sd_setImageWithURL:url
                              placeholderImage:[UIImage imageNamed:@"NoImage.png"]
                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                         cell.image = image;
                                         [cell setup];
                                         [cell.indicator stopAnimating];
                                         
                                         
                                     }];
        }
        else{
            cell.imageView.image = arrDataSource[indexPath.row];
            cell.image = arrDataSource[indexPath.row];
            [cell setup];
        }
        
    }
    
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)_collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    float width = _collectionView.bounds.size.width;
    float height = _collectionView.bounds.size.height;
    
    return CGSizeMake(width, height);
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}



- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if ([collectionView visibleCells].count) {
        for (UICollectionViewCell *cell in [collectionView visibleCells]) {
            NSIndexPath *indexPath = [collectionView indexPathForCell:cell];
             lblTitle.text = [NSString stringWithFormat:@"%ld/%lu",(long)indexPath.row + 1,(unsigned long)arrDataSource.count];
        }
    }
   
}



-(IBAction)closePopUp:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(closePhotoBrowserView)]) {
        [self.delegate closePhotoBrowserView];
    }
    
}

-(void)dealloc{
    
}

@end
