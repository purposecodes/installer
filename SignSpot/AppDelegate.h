//
//  AppDelegate.h
//  SignSpot
//
//  Created by Purpose Code on 09/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "LoginViewController.h"
#import "User.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,LoginDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic,strong) User *currentUser;
@property (readonly, strong, nonatomic)  UINavigationController *navHome;

-(void)checkUserStatus;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
-(void)sendTokenToBackend;
-(void)enablePushNotification;
@end

